main = do
  putStrLn "Hallo. Wie heißt du?"
  name <- getLine
  let ausgabe = "Hallo, " ++ name 
                ++ ". Schön, dich kennen zu lernen!"
  putStrLn ausgabe
