\documentclass{zusammenfassung}
\usepackage{array}
\usepackage{booktabs}
\usepackage{color}
\usepackage{colortbl}
\usepackage{bm}
\usepackage{enumitem}
\usepackage{bbm}
\graphicspath{ {./illustrationen/} }

\newcommand{\R}{{\mathbb R}}

\newtheorem*{algo}{Algorithmus}

\begin{document}
\maketitle{Klasse 11/12}{26. Februar 2016}{2015/2016}

\begin{center}\large\textsc{Lineare Gleichungssysteme}\end{center}

Wir wollen sogenannte \emph{lineare Gleichungssysteme} untersuchen, also Gleichungssysteme der folgenden Form:
\[
  \begin{matrix}
    a_{11}x_1&+&a_{12}x_2&+&\cdots&+&a_{1n}x_n&=&y_1\\
    a_{21}x_1&+&a_{22}x_2&+&\cdots&+&a_{2n}x_n&=&y_2\\
    \vdots&&\vdots&&&&\vdots&&\vdots\\
    a_{m1}x_1&+&a_{m2}x_2&+&\cdots&+&a_{mn}x_n&=&y_m
  \end{matrix}
\]
Dabei sind die $a_{ij}$ und die $y_i$ bekannte Konstanten, und wir wollen Werte für die $x_j$ ermitteln, sodass das
Gleichungssystem gelöst wird.
Um das systematisch zu machen, wollen wir die Variablen $x_j$ und die Konstanten $a_{ij}$ und $y_i$ zuerst in einer
übersichtlichen Weise arrangieren. Wir beginnen mit den Variablen $x_j$.

\begin{center}\large\textsc{Euklidische Räume}\end{center}

Der \emph{$n$-dimensionale euklidische Raum $\R^n$} besteht aus \emph{Tupeln} $\boldsymbol x=(x_1,\ldots,x_n)$, wobei jedes 
$x_i$ eine reelle Zahl ist. Ein solches Tupel $\boldsymbol x$ heißt auch \emph{Vektor}. Wir nennen 
\[
  \boldsymbol e_i:=(\underbrace{0,\ldots,0}_{\text{$(i-1)$-mal}},1,\underbrace{0,\ldots,0}_{\text{$(n-i)$-mal}})
\]
(die $1$ ist im $i$-ten Eintrag) den \emph{$i$-ten Einheitsvektor}. Der \emph{Nullvektor} ist der Vektor
\[
  \boldsymbol 0:=(0,\ldots,0).
\]

\begin{aufgabe*}[Erzeugendensysteme]\leavevmode\label{aufg:erzeugendensysteme}
  \begin{enumerate}[label=\alph*)]
    \item \label{item:erzeugendensysteme} Überlege dir, dass man jeden Vektor $\boldsymbol x$ als Summe 
      \[
        \boldsymbol x=\sum_{j=1}^n\lambda_i\boldsymbol e_i
      \]
      schreiben kann, wobei die $\lambda_i\in\R$ sind. So eine Summe nennt man eine \emph{Linearkombination} der $\boldsymbol
      e_i$.
    \item Eine Familie\footnote{Eine Familie ist so etwas wie eine durchnummerierte Menge: Die Elemente haben einen Index (in
        unserem Fall eine Zahl), und ein Element kann mehrfach auftauchen. Beispielsweise ist $(1,5,3,1)$ eine Familie von
        natürlichen Zahlen, die Vektoren sind Familien von reellen Zahlen, die leere Familie $()$ ist eine Familie von allem
        Möglichen, und $(\boldsymbol e_1,\ldots,\boldsymbol e_n)$ ist eine Familie von Vektoren.} mit der Eigenschaft aus 
      Teil \ref{item:erzeugendensysteme} heißt ein \emph{Erzeugendensystem} von $\R^n$. Wir haben also gezeigt, dass die Familie
      der Einheitsvektor ein Erzeugendensystem von $\R^n$ ist. Finde drei weitere Erzeugendensysteme von $\R^3$. 
    \item Was denkst du: Wie viele Elemente muss ein Erzeugendensystem mindestens haben? 
      Wie viele Elemente kann ein Erzeugendensystem höchstens haben?
  \end{enumerate}
\end{aufgabe*}

\begin{aufgabe}[Basen -- benötigt Aufgabe \ref{aufg:erzeugendensysteme}]\label{aufg:basen}
  Eine \emph{Basis} ist ein \emph{minimales} Erzeugendensystem, also ein Erzeugendensystem $(\boldsymbol b_1,\ldots,\boldsymbol
  b_k)$, sodass $(\boldsymbol b_1,\ldots,\boldsymbol {\hat b_i},\ldots,\boldsymbol b_k)$\footnote{Der Hut bedeutet, dass das
    entsprechende Element ausgelassen wird (eine Tarnkappe sozusagen): So ist beispielsweise $(1,\hat 2,3)=(1,3)$.} für kein $i$
  mehr ein Erzeugendensystem ist. Egal, welches Element man wegnimmt, bekommt man also kein Erzeugendensystem mehr. Zeige, dass
  folgende Aussagen äquivalent sind:
  \begin{enumerate}[label=\roman*)]
    \item $(\boldsymbol b_1,\ldots,\boldsymbol b_k)$ ist ein Erzeugendensystem von $\R^n$,
    \item \label{item:charakterisierung-basis}
      Jedes Element $\boldsymbol x\in\R^n$ lässt sich auf \emph{eindeutige} Weise als Linearkombination der $\boldsymbol b_i$
      schreiben,
    \item Die Familie $(\boldsymbol b_i)$\footnote{Das ist nur eine andere Schreibweise für $(\boldsymbol b_1,\ldots,\boldsymbol
        b_n)$} ist \emph{linear unabhängig}, das heißt: Aus
      \[
        \boldsymbol 0=\sum_{j=1}^n\lambda_i\boldsymbol e_i
      \]
      folgt stets, dass alle $\lambda_i=0$ sind.
  \end{enumerate}
  Benutze Teil \ref{item:charakterisierung-basis}, um zu zu zeigen, dass $(\boldsymbol e_i)$ eine Basis von
  $\R^n$ ist. Man nennt $(\boldsymbol e_i)$ auch die \emph{Standardbasis} von $\R^n$ und $\boldsymbol e_i$ den \emph{$i$-ten
    Standardbasisvektor}.
\end{aufgabe}

Man kann zeigen, dass jede Basis von $\R^n$ die gleiche Anzahl an Elementen (nämlich $n$ Stück) haben muss. Das ist allerdings
nicht so leicht, und wir brauchen das auch nicht.

\begin{center}\large\textsc{Lineare Abbildungen}\end{center}

Unsere Variablen $x_i$ lassen sich also zu einem Vektor $\boldsymbol x=(x_1,\ldots,x_n)\in\R^n$ zusammenfassen. Tatsächlich ist
der Unterschied zwischen den Variablen $x_i$ und den Konstanten $y_j$ nicht so groß, denn wir können auch die Konstanten $y_j$ zu
einem Vektor $\boldsymbol y=(y_1,\ldots,y_m)\in\R^m$ zusammenfassen.

Wir wechseln kurz den Sichtpunkt auf das lineare Gleichungssystem vom Beginn: Angenommen, wir kennen die Konstanten $a_{ij}$ und
die $x_j$ bereits. Dann können wir die $y_i$ einfach ermitteln, indem wir die linke Seite der Gleichungen ausrechnen. Wenn wir die
$a_{ij}$ festhalten, bekommen wir also eine Abbildung
\[
  A\colon\R^n\to\R^m,\quad
  A(\boldsymbol x)=A(x_1,\ldots x_n):=(a_{11}x_1+\ldots+a_{1n}x_n,\ldots,a_{m1}x_1+\ldots+a_{mn}x_n).
\]

Mit Hilfe dieser Abbildung können wir auch unsere ursprüngliches Problem besser verstehen: Wir haben einen konkreten Vektor
$\boldsymbol y\in\R^m$ gegeben, und suchen einen Vektor $\boldsymbol x\in\R^n$, der das "`Gleichungssystem"'
\[
  A(\boldsymbol x)=\boldsymbol y
\]
löst. Abbildungen, die wie $A$ entstehen, heißen \emph{lineare Abbildungen}. Man kann lineare Abbildungen auch anders beschreiben:

\begin{aufgabe}[Lineare Abbildungen -- benötigt Aufgabe \ref{aufg:erzeugendensysteme}]\label{aufg:lineare-abbildungen}
  Sei $A\colon\R^n\to\R^m$ eine Abbildung. Zeige, dass folgende Aussagen äquivalent sind:
  \begin{enumerate}[label=\roman*)]
    \item $A$ ist eine lineare Abbildung, das heißt, es gibt Zahlen $a_{ij}$, sodass $A$ die Form
      \[
        A(x_1,\ldots,x_n)=(a_{11}x_1+\ldots+a_{1n}x_n,\ldots,a_{m1}x_1+\ldots+a_{mn}x_n)
      \]
      hat.
    \item Für alle Zahlen $\lambda,\mu\in\R$ und alle Vektoren $\boldsymbol w,\boldsymbol x\in\R^n$ gilt die Gleichung
      \[
        A(\lambda\boldsymbol w+\mu\boldsymbol x)=\lambda A(\boldsymbol w)+\mu A(\boldsymbol x).
      \]
      Dabei ist $\lambda (x_1,\ldots,x_n):=(\lambda x_1,\ldots,\lambda x_n)$ die \emph{skalare Multiplikation} und 
      \[
        \boldsymbol w+\boldsymbol x=(w_1,\ldots,w_n)+(x_1,\ldots,x_n):=(w_1+x_1,\ldots,w_n+x_n)
      \]
      die \emph{Vektoraddition}.
  \end{enumerate}
  \emph{Tipp: Für die Richtung $\text{ii)}\Rightarrow\text{i)}$ überlege dir, was $A(\boldsymbol e_i)$ sein müsste, wenn $A$ die
    Form aus Teil i) hätte. Aus dieser Information bekommst du die Zahlen $a_{ij}$. Jetzt musst noch zeigen, dass die Abbildung,
    die mit Hilfe der Zahlen $a_{ij}$ definiert wurde mit der Abbildung aus Teil ii) für beliebige Vektoren übereinstimmt. Dafür
    kannst du verwenden, dass $(\boldsymbol e_i)$ ein Erzeugendensystem von $\R^n$ ist.}
\end{aufgabe}

\begin{center}\large\textsc{Matrizen}\end{center}

Lineare Abbildungen sind per definitionem dadurch bestimmt, dass man die Zahlen $a_{ij}$ vorgibt. Oft schreibt man diese Zahlen in
ein Schema wie folgt:
\[
  \begin{pmatrix}
    a_{11}&a_{12}&\cdots&a_{1n}\\
    a_{21}&a_{22}&\cdots&a_{2n}\\
    \vdots&\vdots&&\vdots\\
    a_{m1}&a_{m2}&\cdots&a_{mn}
  \end{pmatrix}
\]

So ein Schema heißt eine \emph{Matrix}, und man bezeichnet diese Matrix üblicherweise mit demselben Buchstaben wie die lineare
Abbildung, also in unserem Fall mit $A$.
Da Matrizen und lineare Abbildungen im Prinzip dasselbe sind, unterscheidet man auch in der Notation nicht. Man kann also eine
Matrix auf einen Vektor anwenden und bekommt wieder einen Vektor.

\begin{aufgabe}[Erste Matrizenrechnung]
  Wir schreiben $\boldsymbol x:=(2,5)\in\R^2$. Berechne:
  \begin{enumerate}
    \item $\displaystyle\begin{pmatrix}2&5\\3&1\end{pmatrix}\boldsymbol x$,
    \item $\displaystyle\begin{pmatrix}-1&1\\0&5\\4&2\end{pmatrix}\boldsymbol x$,
    \item $\displaystyle\begin{pmatrix}0&1\\1&0\end{pmatrix}\boldsymbol x$.
  \end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Fortgeschrittene Matrizenrechnung]
  Zeige, dass folgende Abbildungsvorschriften lineare Abbildungen definieren, indem du sie durch Matrizen darstellst. Überlege dir
  dafür erst, was die Definitions- und was die Zielmenge der jeweiligen Abbildungen ist:
  \begin{enumerate}
    \item Multiplikation des ersten Eintrags eines Vektors aus $\R^3$ mit $\pi$,
    \item Vertauschen des zweiten und des dritten Eintrags eines Vektors aus $\R^4$,
    \item Vergessen der ersten Komponente eines Vektors aus $\R^2$,
    \item Addieren der zweiten Zeile eines Vektors aus $\R^3$ zu der ersten Zeile,
    \item Berechnen der Summe der Einträge eines Vektors aus $\R^6$.
    \item Die Abbildung, die nichts ändert: Sie schickt einen Vektor $\boldsymbol x\in\R^n$ auf $\boldsymbol x\in\R^n$. Diese
      Abbildung heißt die \emph{Identität}, und wird $\text{id}$ oder $\mathbbm 1$ geschrieben. Die zugehörige Matrix heißt die
      \emph{Einheitsmatrix}.
  \end{enumerate}
\end{aufgabe}

Matrizen haben den großen Vorteil, dass man Operationen, die man an linearen Abbildungen durchführt, auf einfache Art und Weise
durch Matrizen darstellen kann. Es gibt im Wesentlichen drei solche Operationen:
\begin{itemize}
  \item Man kann lineare Abbildungen addieren: Wenn $A,B\colon\R^n\to\R^m$ lineare Abbildungen sind, dann ist ihre \emph{Summe}
    $A+B\colon\R^n\to\R^m$ durch die Abbildungsvorschrift
    \[
      (A+B)(\boldsymbol x):=A(\boldsymbol x)+B(\boldsymbol y)
    \]
    gegeben.
  \item Man kann lineare Abbildungen skalar multiplizieren: Wenn $A\colon\R^n\to\R^m$ eine lineare Abbildung und $\lambda\in\R$
    eine reelle Zahl (ein \emph{Skalar}) ist, dann ist $\lambda A\colon\R^n\to\R^m$ durch die Abbildungsvorschrift
    \[
      (\lambda A)(\boldsymbol x):=\lambda A(\boldsymbol x)
    \]
    definiert.
  \item Man kann lineare Abbildungen miteinander verketten: Für zwei lineare Abbildungen
    $A\colon\R^n\to\R^m$ und $B\colon\R^m\to\R^k$
    ist ihre \emph{Verkettung} (oder \emph{Verknüpfung}, oder \emph{Hintereinanderschaltung}, oder \emph{Komposition})
    die lineare Abbildung $B\circ A\colon\R^n\to\R^k$, die durch die Abbildungsvorschrift
    \[
      (B\circ A)(\boldsymbol x):=B(A(\boldsymbol x))
    \]
    gegeben ist.
\end{itemize}

\begin{aufgabe}[Operationen I]
  Zeige, dass $A+B$, $\lambda A$ und $B\circ A$ tatsächlich lineare Abbildungen sind, indem du Charakterisierung ii) aus Aufgabe
  \ref{aufg:lineare-abbildungen} verwendest.
\end{aufgabe}

\begin{aufgabe}[Operationen II]
  Es seien $A,B\colon\R^2\to\R^2$ lineare Abbildungen, die durch die Matrizen
  \[
    A=\begin{pmatrix}1&2\\0&1\end{pmatrix}\quad\text{und}\quad B=\begin{pmatrix}-1&1\\1&2\end{pmatrix}
  \]
  gegeben sind. Es sei $\boldsymbol x=(x_1,x_2)\in\R^2$ ein allgemeiner Vektor. Berechne $(A+B)(\boldsymbol x)$, $(\lambda
  A)(\boldsymbol x)$, $(B\circ A)(\boldsymbol x)$ und $(A\circ B)(\boldsymbol x)$. Was sind also die Matrizen, die zu $A+B$,
  $\lambda A$, $B\circ A$ und $A\circ B$ gehören?
\end{aufgabe}

\begin{aufgabe}[Operationen III]
  Finde Formeln für Addition, Multiplikation und Komposition von Matrizen. Komposition von Matrizen wird übrigens oft auch
  \emph{Multiplikation} von Matrizen genannt, und das Zeichen $\circ$ wird meistens weggelassen.
\end{aufgabe}

\begin{aufgabe}[Operationen IV]
  Oft ist es sinnvoll, einen Vektor $\boldsymbol x=(x_1,\ldots,x_n)\in\R^n$ als Matrix
  \[
    \boldsymbol x=\begin{pmatrix}x_1\\\vdots\\x_n\end{pmatrix}
  \]
  aufzufassen.
  \begin{enumerate}
    \item Was für eine lineare Abbildung wird durch den Vektor $\boldsymbol x$ dargestellt.
    \item Sei $A\colon\R^n\to\R^m$ eine lineare Abbildung. Was für eine Abbildung ist dann $A\circ\boldsymbol x=A\boldsymbol x$?
  \end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Matrimultiplikation]
  Berechne folgende Matrix-Produkte:
  \begin{enumerate}
    \item $\displaystyle\begin{pmatrix}
        3&4&5
      \end{pmatrix}\begin{pmatrix}
        2\\8\\7
      \end{pmatrix}$
    \item $\displaystyle\begin{pmatrix}
        2\\8\\7
      \end{pmatrix}\begin{pmatrix}
        3&4&5
      \end{pmatrix}$
    \item $\displaystyle\begin{pmatrix}
        -1&1\\
        2&1
      \end{pmatrix}\begin{pmatrix}
        3&6&-3\\2&1&0
      \end{pmatrix}$
    \item $\displaystyle\begin{pmatrix}
        1&0&0\\
        0&1&1\\
        1&0&1\\
        0&1&1
      \end{pmatrix}\begin{pmatrix}
        2&7\\
        5&1\\
        4&4
      \end{pmatrix}$
    \item $\displaystyle\begin{pmatrix}
        1&2&4&1&2\\
        3&5&3&-1&1\\
        2&3&2&1&5
      \end{pmatrix}\begin{pmatrix}
        0\\1\\0\\0\\0
      \end{pmatrix}$
  \end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Elementarmatrizen I]\label{aufg:elementarmatrizen-I}
  Finde Matrizen $P_{jk}$, $Z_j(\lambda)$, $S_{jk}(\lambda)$ (wobei $j\neq k$ und $0\neq\lambda\in\R$), sodass folgende
  Eigenschaften für jede Matrix $A$ mit $m$ Zeilen und $n$ Spalten erfüllt sind:
  \begin{enumerate}
    \item $P_{jk}\circ A$ ist die Matrix, bei der die $j$-te und die $k$-te Zeile von $A$ vertauscht wurden,
    \item $Z_j(\lambda)\circ A$ ist die Matrix, bei der die $j$-te Zeile von $A$ mit $\lambda$ multipliziert wurde,
    \item $S_{jk}(\lambda)\circ A$ ist die Matrix, bei der das $\lambda$-fache der $k$-ten Zeile von $A$ zur $j$-ten Zeile von $A$
      hinzuaddiert wurde.
  \end{enumerate}
  \emph{Tipp: Versuche die Aufgabe erst für $m=n=2$, dann für $m=3$ und $n=2$, dann für beliebige $m$ und dann für beliebige $n$.}
\end{aufgabe}

Eine quadratische Matrix $A$ heißt \emph{invertierbar}, falls eine andere Matrix $B$ existiert, sodass $AB=\mathbbm 1$ und
$BA=\mathbbm 1$ gilt. Man schreibt in diesem Fall $A^{-1}:=B$.

\begin{aufgabe}[Elementarmatrizen II -- benötigt Aufgabe \ref{aufg:elementarmatrizen-I}]
  Überlege dir, dass die Elementarmatrizen aus \ref{aufg:elementarmatrizen-I} invertierbar sind, wobei die inversen Matrizen
  wieder Elementarmatrizen sind.
\end{aufgabe}

\begin{center}\large\textsc{Das Gauß-Verfahren}\end{center}

\setcounter{MaxMatrixCols}{15}
Eine Matrix $A$ hat \emph{Zeilenstufenform}, wenn sie wie folgt aussieht:
\[
  A=
  \begin{pmatrix}
    0&\cdots&0&a_1&*&\cdots\\
    0&&&\cdots&&0&a_2&*&\cdots\\
    &&&&&&0\\
    \vdots&&&&&&&\ddots\\
    &&&&&&&&0&a_r&*&\cdots&*\\
    &&&&&&&&&0&\cdots&\cdots&0\\
    \vdots&&&&&&&&&&&&\vdots
  \end{pmatrix}
\]
Jede Zeile hat also mehr Nullen am Anfang als die vorhergehende Zeile, bis nur noch Nullzeilen vorkommen. Folgende Matrix ist in
Zeilenstufenform:
\[
  \begin{pmatrix}
    4&8&3\\
    0&1&4\\
    0&0&0\\
    0&0&0
  \end{pmatrix}
\]
Folgende Matrix ist nicht in Zeilenstufenform:
\[
  \begin{pmatrix}
    1&0&0\\
    0&1&0\\
    0&1&0\\
    0&0&1
  \end{pmatrix}
\]

\begin{aufgabe}[Lineare Gleichungssysteme lösen I]\label{aufg:lineare-gleichungssysteme-lösen-I}
  Sei $A$ eine Matrix in Zeilenstufenform, und $\boldsymbol b$ ein Vektor. 
  Gib ein Verfahren an, mit dem du das Gleichungssystem $A\boldsymbol x=\boldsymbol b$
  lösen kannst.
\end{aufgabe}

\begin{aufgabe}[Lineare Gleichungssysteme lösen II -- benötigt Aufgabe \ref{aufg:lineare-gleichungssysteme-lösen-I}]
  \label{aufg:lineare-gleichungssysteme-lösen-II}
  Sei $B$ eine invertierbare Matrix, sodass $BA$ in Zeilenstufenform ist. Gib ein Verfahren an, mit dem du das Gleichungssystem
  $A\boldsymbol x=\boldsymbol b$ lösen kannst.
\end{aufgabe}

Wir müssen also einen Weg finden, so eine Matrix $B$ wie in Aufgabe \ref{aufg:lineare-gleichungssysteme-lösen-II} zu finden.
Dieser Weg ist das sogenannte \emph{Gauß-Verfahren}:

\begin{algo}[Gauß-Verfahren]\leavevmode
  \begin{enumerate}
    \item Gehe in die erste Spalte von $A$, die einen Eintrag hat, der nicht Null ist. Diese Spalte sei die $k$-te Spalte.
    \item Es sei die $j$-te Zeile die oberste Zeile, in der $a_{jk}\neq 0$ ist. Vertausche die erste und die $j$-te Zeile von
      $A$ (das Ergebnis ist $A':=P_{jk}A=(a_{jk}')$). 
      Jetzt ist die $k$-te Spalte von $A'$ die erste von Null verschiedene Spalte und $A'$
      hat einen von Null verschiedenen Eintrag in der ersten Zeile in der $k$-ten Spalte (nämlich $a_{1k}'=a_{jk}$).
    \item Für alle Zeilen von $A'$ unter der ersten Zeile führe folgende Transformation aus: In der $j$-ten Zeile addiere 
      das $-a_{jk}'(a_{1k}')^{-1}$-fache der ersten Zeile zur $j$-ten Zeile dazu (das Ergebnis ist
      $A'':=S_{j1}(-a_{jk}'(a_{1k}')^{-1})A$).
    \item Die Matrix $A''$ hat nun die Form
      \[
        A''=
        \begin{pmatrix}
          0&\cdots&0&a_{1k}'&*&\cdots&*\\
          0&\cdots&0&0\\
          \vdots&&&\vdots&&B\\
          0&\cdots&&0
        \end{pmatrix}
      \]
      Wende jetzt den Algorithmus auf $B$ an.
    \item Der Algorithmus endet, wenn die Matrix in Zeilenstufenform ist.
  \end{enumerate}
\end{algo}

\begin{aufgabe}[Gauß-Algorithmus I]\label{aufg:gauß-algorithmus-I}
  Führe den Gauß-Algorithmus mit der Matrix
  \[
    A:=
    \begin{pmatrix}
      0&0&2&3\\
      0&1&1&1\\
      0&2&3&4\\
      0&3&0&5
    \end{pmatrix}
  \]
  aus. Notiere dir jeweils, welche Transformationen du durchgeführt hast.
\end{aufgabe}

\begin{aufgabe}[Gauß-Algorithmus II]
  Überlege dir, warum der Gauß-Algorithmus immer endet.
\end{aufgabe}

\begin{aufgabe}[Gauß-Algorithmus III -- benötigt Aufgaben \ref{aufg:lineare-gleichungssysteme-lösen-II} und
  \ref{aufg:gauß-algorithmus-I}]
  Löse das Gleichungssystem
  \[
    \begin{pmatrix}
      0&0&2&3\\
      0&1&1&1\\
      0&2&3&4\\
      0&3&0&5
    \end{pmatrix}
    \begin{pmatrix}
      x_1\\x_2\\x_3\\x_4
    \end{pmatrix}
    =
    \begin{pmatrix}
      5\\3\\9\\8
    \end{pmatrix}.
  \]
\end{aufgabe}

\end{document}
