\documentclass{zusammenfassung}
\usepackage{array}
\usepackage{booktabs}
\usepackage{color}
\usepackage{colortbl}
\graphicspath{ {./illustrationen/} }

\begin{document}
\maketitle{Klasse 11/12}{27. November 2015}{2015/2016}

\begin{center}\large\textsc{Komplexe Zahlen}\end{center}

Die Gleichung $x^2=-1$ hat keine Lösung $x\in\mathbb R$. Um die Gleichung trotzdem lösen zu können, führen wir eine "`neue Zahl"'
$i$ ein, für die $i^2=-1$ gilt. Um damit rechnen zu können, gibt es folgende Rechenregeln:

\begin{enumerate}
	\item Man kann $i$ mit einer reellen Zahl $\lambda$ multiplizieren. Das Ergebnis schreibt man einfach als $\lambda i$.
	\item Man kann auch $\lambda i$ zu einer reellen Zahl $\mu$ addieren. Das Ergebnis schreibt man als $\mu+\lambda i$.
\end{enumerate}

Eine \emph{komplexe Zahl} ist eine Zahl der Form $\mu+\lambda i$, wobei $\mu,\lambda\in\mathbb R$ reelle Zahlen sind. Man
bezeichnet komplexe Zahlen üblicherweise auch mit einzelnen Buchstaben, also zum Beispiel $z=\mu+\lambda i$. Die Menge der
komplexen Zahlen bezeichnen wir mit $\mathbb C$.

\begin{aufgabe}[Addition und Multiplikation]\label{addition-und-multiplikation}
	Wir wollen, dass das Assoziativgesetz, das Kommutativgesetz und das Distributivgesetz auch für komplexe Zahlen gilt. Überlege
	dir auf dieser Grundlage, dass die Summe und das Produkt zweier komplexer Zahlen wieder komplexe Zahlen sind.

	\emph{Tipp: Schreibe $z:=\lambda+\mu i$ und $w:=\alpha+\beta i$. Schreibe $z+w$ und $z\cdot w$ aus und verwende die
		Rechenregeln, um das Ergebnis auf die Form $\gamma+\delta i$ zu bringen.}
\end{aufgabe}

\begin{aufgabe}[Komplexe Konjugation, Betrag und Zahlenebene -- benötigt Aufgabe \ref{addition-und-multiplikation}]
	\label{komplexe-konjugation-betrag-und-zahlenebene}
	Für eine komplexe Zahl $z=\mu+\lambda i$ ist $\bar\mu:=\mu-\lambda i$ die \emph{komplex Konjugierte} zu $z$.
	\begin{enumerate}
		\item	Für eine komplexe Zahl $z=\mu+\lambda i$ heißt $\mu=\text{Re}(z)$ der \emph{Realteil} und $\lambda=\text{Im}(z)$ der
			\emph{Imaginärteil} von $z$. Überlege dir, dass
			\[
				\text{Re}(z)=\frac 12(z+\bar z)\quad\text{und}\quad\text{Im}(z)=\frac 12(z-\bar z)
			\]
			gilt.
		\item Man kann eine komplexe Zahl in die Ebene $\mathbb R^2$ einzeichnen, indem man $\text{Re}(z)$ als die $x$-Koordinate und
			$\text{Im}(z)$ als die $y$-Koordinate von $z$ interpretiert. Zeichne die Zahlen $i$, $-i$ und $1+2i$ in die Ebene ein. Wegen 
			dieser Interpretation nennt man $\mathbb C$ auch die \emph{komplexe Zahlenebene}.
		\item Überprüfe, dass für jede Zahl $z\in\mathbb C$ gilt, dass $z\bar z\in\mathbb R_{\geq 0}$.
		\item Da $z\bar z\in\mathbb R_{\geq 0}$ ist, können wir daraus die Wurzel ziehen: Der \emph{Betrag} von $z$ ist die reelle
			Zahl $|z|:=\sqrt{z\bar z}$. Überlege dir, dass $|z|$ den Abstand von $z$ zum Ursprung in der komplexen Zahlenebene misst.
	\end{enumerate}
\end{aufgabe}

\begin{center}\large\textsc{Komplexe Ableitung}\end{center}

Für Funktionen $f:\mathbb R\to\mathbb R$ war ja die Ableitung an einem Punkt $p\in\mathbb R$ durch den Differenzialquotienten
\[
	f'(p)=\lim_{\substack{h\to 0\\h\neq 0}}\frac{f(p+h)-f(p)}h
\]
gegeben, wenn dieser Grenzwert existiert. Man kann diese Gleichung auch umformulieren, indem man
\[
	\phi(h):=\frac{f(p+h)-f(p)}h
\]
für alle $h\neq 0$ schreibt. Da $\lim_{h\to 0, h\neq 0}\phi(h)=f'(p)$ ist, kann $\phi$ stetig auf ganz $\mathbb R$ durch
$\phi(0)=f'(p)$ fortgesetzt werden. Dann ist natürlich
\begin{equation}\label{eq:differenzierbarkeit}
	f(p+h)=f(p)+\phi(h)h.
\end{equation}
Falls umgekehrt eine stetige Funktion $\phi:\mathbb R\to\mathbb R$ existiert, sodass Gleichung \eqref{eq:differenzierbarkeit}
erfüllt ist, dann kann man die ganze Rechnung rückwärts durchführen und erhält, dass $f$ bei $p$ differenzierbar ist mit
$f'(p)=\phi(0)$. Die Gleichung \eqref{eq:differenzierbarkeit} hat allerdings gegenüber dem Differenzialquotienten den Vorteil,
dass keine Division mehr benötigt ist. Da wir noch nicht wissen, wie man komplexe Zahlen dividiert, können wir also Gleichung
\eqref{eq:differenzierbarkeit} als unsere Definition für komplexe Differenzierbarkeit ansehen:

Eine Funktion $f:\mathbb C\to\mathbb C$ heißt in $p\in\mathbb C$ \emph{komplex differenzierbar}, falls eine
stetige Funktion $\phi:\mathbb C\to\mathbb C$ existiert, sodass Gleichung \eqref{eq:differenzierbarkeit} für alle $h\in\mathbb C$
erfüllt ist. Die \emph{Ableitung} von $f$ in $p$ ist $f'(p):=\phi(0)$. $f$ heißt \emph{holomorph}, falls $f$ an jedem Punkt
$p\in\mathbb C$ komplex differenzierbar ist. Wenn $U\subset\mathbb C$ eine Teilmenge ist, sodass $f$ in jedem Punkt von $U$
komplex differenzierbar ist, dann heißt $f$ \emph{holomorph} auf $U$.

\begin{aufgabe}[Beispiele holomorpher Funktionen]
	Zeige, dass die folgenden Funktionen holomorph sind, und berechne die Ableitungen:
	\begin{enumerate}
		\item $f(z):=z$,
		\item $f(z):=z_0$, wobei $z_0\in\mathbb C$ eine Konstante ist,
		\item $f(z):=\lambda z$, wobei $\lambda\in\mathbb C$ eine Konstante ist
	\end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Beispiel einer nicht holomorphen Funktion -- benötigt Aufgabe \ref{komplexe-konjugation-betrag-und-zahlenebene}]
	Zeige, dass die Funktion $f(z)=\bar z$ \emph{nirgends} komplex differenzierbar ist.

	\emph{Tipp: Nimm an, dass Gleichung \eqref{eq:differenzierbarkeit} für $f$ in $p$ erfüllt ist. Überlege dir, was für einen Wert
		$\phi$ haben muss, wenn $\text{Re}(h)=0$, und wenn $\text{Im}(h)=0$. Überlege dir, warum das ein Widerspruch zu der Stetigkeit
		von $\phi$ bei $0$ ist.}
\end{aufgabe}

\begin{aufgabe}[Summen- und Produktregel]\label{summen-und-produktregel}
	Es seien $f$ und $g$ in $p$ komplex differenzierbar. Beweise folgende Aussagen:
	\begin{enumerate}
		\item $f+g$ ist in $p$ komplex differenzierbar mit $(f+g)'(p)=f'(p)+g'(p)$,
		\item $f\cdot g$ ist in $p$ komplex differenzierbar mit $(f\cdot g)'(p)=f'(p)g(p)+f(p)g'(p)$.
	\end{enumerate}
	
	\emph{Tipp: Versuche, die Definition der Differenzierbarkeit von $f+g$ bzw. $f\cdot g$ direkt nachzurechnen und setze an
		geeigneter Stelle die Definition der Differenzierbarkeit von $f$ und $g$ ein.}
\end{aufgabe}

\begin{aufgabe}[Kettenregel]
	Es sei $f$ in $p$ und $g$ in $f(p)$ komplex differenzierbar. Dann ist $g\circ f(z):=g(f(z))$ in $p$ komplex differenzierbar mit
	\[
		(g\circ f)'(p)=g'(f(p))\cdot f'(p).
	\]
\end{aufgabe}

\begin{aufgabe}[Polynome -- benötigt Aufgabe \ref{summen-und-produktregel}]
	Zeige folgende Aussagen:
	\begin{enumerate}
		\item Wenn $f(z)=z^n$, dann ist $f$ holomorph und $f'(z)=nz^{n-1}$.

			\emph{Tipp: Verwende die Produktregel und vollständige Induktion -- nimm also an, dass die Aussage für $n-1$ anstelle von
				$n$ bereits wahr ist.}
		\item Wenn $f(z)=a_0+a_1z+a_2z^2+\ldots+a_nz^n$ ist, dann ist $f$ holomorph und
			\[
				f'(z)=a_1+2a_2z+3a_3z^2+\ldots+na_nz^{n-1}.
			\]
	\end{enumerate}
\end{aufgabe}

\begin{center}\large\textsc{Potenzreihen}\end{center}

Eine \emph{Potenzreihe} mit Entwicklungspunkt $z_0\in\mathbb C$ ist eine unendliche Summe der Form
\[
	\sum_{k=0}^\infty a_k(z-z_0)^k.
\]
Der \emph{Grenzwert} einer solchen Potenzreihe bei einer Zahl $w\in\mathbb C$ ist eine Zahl $L\in\mathbb C$, sodass
\[
	\left|\sum_{k=0}^Na_k(w-z_0)^k-L\right|
\]
für große natürliche Zahlen $N\in\mathbb N$ beliebig klein wird. Dieser Grenzwert wird auch mit dem Summenzeichen bezeichnet, also
\[
	L=\sum_{k=0}^\infty a_k(w-z_0)^k.
\]
Falls der Grenzwert in jedem Punkt einer Menge $U\subset\mathbb C$ existiert, dann definiert so eine Potenzreihe eine Funktion
\[
	f:U\to\mathbb C,\quad f(z):=\sum_{k=0}^\infty a_k(z-z_0)^k.
\]

\begin{aufgabe}[Beispiele für Grenzwerte von Potenzreihen]
	Zeige folgende Behauptungen:
	\begin{enumerate}
		\item Für jede Potenzreihe $\sum_{k=0}^\infty a_k(z-z_0)^k$ ist der Grenzwert bei $w=z_0$ gleich $a_0$.
		\item \emph{Geometrische Reihe.} Es ist
			\[
				\sum_{k=0}^{N-1}z^k=\frac{1-z^N}{1-z}
			\]
		\item Falls $|z|<1$ ist, dann ist $\lim_{N\to\infty}z^N=0$. Folgere daraus, dass
			\[
				\sum_{k=0}^\infty z^k=\frac 1{1-z}
			\]
			für $|z|<1$.
	\end{enumerate}
\end{aufgabe}

Wenn Potenzreihen in einer Umgebung von $z_0$ \emph{konvergieren}, also die Grenzwerte dort existieren, dann kann man die
Potenzreihe bei $z_0$ ableiten:

\begin{aufgabe}[Ableitung von Potenzreihen]\label{ableitung-von-potenzreihen}
	Wenn $f(z)=\sum_{k=0}^\infty a_k(z-z_0)^k$ ist, dann ist $f$ bei $z_0$ komplex differenzierbar mit
	\[
		f'(z)=\sum_{k=0}^\infty (k+1)a_{k+1}(z-z_0)^k.
	\]

	\emph{Bemerkung: Die Aufgabe klingt auf den ersten Blick leicht. Sie ist aber tatsächlich ziemlich schwer, weil man sich stark
		konzentrieren muss, um zu sehen, dass man die Ableitungen und die Grenzwertbildung miteinander vertauschen darf.}
\end{aufgabe}

Man kann Potenzreihen auch multiplizieren (das funktioniert nur, wenn die Potenzreihen in einer Umgebung des Punktes
konvergieren):
\begin{equation}\label{eq:cauchy-produkt}
	\left(\sum_{k=0}^\infty a_k(z-z_0)^k\right)\left(\sum_{l=0}^\infty b_l(z-z_0)^l\right)
	=\sum_{n=0}^\infty\left(\sum_{k+l=n}a_kb_l\right)(z-z_0)^n.
\end{equation}

\begin{aufgabe}[Cauchy-Produkt von Reihen]
	Überlege dir anschaulich, warum die Formel \eqref{eq:cauchy-produkt} Sinn ergibt.
\end{aufgabe}

\begin{aufgabe}[Die Exponentialfunktion -- benötigt Aufgabe \ref{ableitung-von-potenzreihen}]\label{die-exponentialfunktion}
	Wir schreiben
	\[
		\exp(z):=\sum_{k=0}^\infty\frac 1{k!}z^k.
	\]
	\begin{enumerate}
		\item Zeige, dass $\exp'(z)=\exp(z)$.
		\item Berechne $\exp(0)$.
		\item Überlege dir, dass $\exp(z+w)=\exp(z)\cdot\exp(w)$.
	\end{enumerate}

	{
		\slshape Tipp: Verwende für den letzten Teil die Gleichung \eqref{eq:cauchy-produkt} und die folgenden Formeln:
		\[
			(a+b)^n=\sum_{k=0}^n\binom nka^kb^{n-k}\quad\text{und}\quad\binom nk=\frac{n!}{k!(n-k)!}.
		\]
	}
\end{aufgabe}

\begin{aufgabe}[Sinus und Kosinus -- benötigt Aufgabe \ref{die-exponentialfunktion}]
	\label{sinus-und-kosinus}
	Wir schreiben
	\[
		\sin(z):=\frac 1{2i}(\exp(iz)-\exp(-iz))\quad\text{und}\quad\cos(z):=\frac 12(\exp(iz)+\exp(-iz)).
	\]
	\begin{enumerate}
		\item Zeige $\sin'(z)=\cos(z)$ und $\cos'(z)=-\sin(z)$.
		\item Berechne $\sin(0)$ und $\cos(0)$.
		\item Zeige $\exp(iz)=\cos(z)+i\sin(z)$.
		\item Berechne $\sin(z+w)$ und $\cos(z+w)$ in Abhängigkeit von $\sin(z)$, $\sin(w)$, $\cos(z)$ und $\cos(w)$, indem du das
			Additionstheorem $\exp(z+w)=\exp(z)\exp(w)$ verwendest.
		\item Zeige $\sin(z)^2+\cos(z)^2=1$.
		\item Zeige $\sin(-z)=-\sin(z)$ und $\cos(-z)=\cos(z)$.
	\end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Multiplikation auf dem Kreis -- benötigt Aufgaben \ref{komplexe-konjugation-betrag-und-zahlenebene},
	\ref{die-exponentialfunktion} und \ref{sinus-und-kosinus}]
	Der \emph{Einheitskreis} ist die Menge
	\[
		S^1:=\{z\in\mathbb C\mid |z|=1\}.
	\]
	Wir wollen untersuchen, was die Multiplikation auf $S^1$ macht.
	\begin{enumerate}
		\item Überlege dir, dass für jede reelle Zahl $t\in\mathbb R$ gilt, dass $|\exp(it)|=1$, also $\exp(it)\in S^1$.
		\item Überlege dir anschaulich, dass man jede Zahl $z\in S^1$ als $z=\exp(it)$ für ein $t\in\mathbb R$ schreiben kann.

			\emph{Tipp: Verwende Sinus und Kosinus von dem Winkel, den die Zahl $z$ zur $x$-Achse bildet.}
		\item Was passiert anschaulich, wenn man die Zahlen $z$ und $w$ ($z,w\in S^1$) multipliziert?

			\emph{Tipp: Schreibe $z=\exp(it)$ und $w=\exp(is)$. Was für eine anschauliche Bedeutung haben die Zahlen $s$ und $t$? Was
				ist $zw$?}
		\item Welche Zahl $z^{-1}$ erfüllt $z^{-1}z=1$, wenn $z\in S^1$ ist?
		\item Wie kann für beliebige Zahlen $z\in\mathbb C\setminus\{0\}$ das multiplikative Inverse berechnen, also eine Zahl
			$z^{-1}$, sodass $z^{-1}z=1$ ist?
	\end{enumerate}
\end{aufgabe}

\pagebreak

\begin{center}\large\textsc{Integration über Kurven}\end{center}

Wir wollen Funktionen $f:[a,b]\to\mathbb C$ integrieren. Mann kann $f=g+ih$ schreiben, wobei $g,h\colon[a,b]\to\mathbb R$
Funktionen sind. Das \emph{Integral} einer solchen Funktion definieren wir als
\[
	\int_a^b\gamma(t)\,dt:=\int_a^bg(t)\,dt+i\int_a^bh(t)\,dt,
\]
falls wir $g$ und $h$ integrieren können.

\begin{aufgabe}[Einfache Integrationsaufgaben]
	Berechne die Integrale folgender Funktionen:
	\begin{enumerate}
		\item $f\colon[0,1]\to\mathbb C$, $f(t):=3t+i(4t+t^2)$,
		\item $f\colon[0,2\pi]\to\mathbb C$, $f(t):=\cos(t)+i\sin(t)$,
		\item $f\colon[1,7]\to\mathbb C$, $f(t):=t+it^2$.
	\end{enumerate}
\end{aufgabe}

Wie für reelle Integrale gilt auch hier die Substitutionsregel
\[
	\int_a^bf(s)\,ds=\int_\alpha^\beta f(h(t))h'(t)\,dt,
\]
falls $h\colon[\alpha,\beta]\to[a,b]$ stückweise stetig differenzierbar und umkehrbar ist.

Eine \emph{Kurve} in $\mathbb C$ ist eine Funktion $\gamma=\zeta+i\theta\colon[a,b]\to\mathbb C$, sodass
$\zeta,\theta\colon[a,b]\to\mathbb R$ stückweise stetig differenzierbar sind. Wenn $f\colon\mathbb C\to\mathbb C$ eine Funktion
ist, dann definieren wir das Integral von $f$ entlang $\gamma$ durch
\[
	\int_\gamma f(z)\,dz:=\int_a^bf(\gamma(t))\gamma'(t)\,dt.
\]

\begin{aufgabe}[Ein paar Kurvenintegrale -- benötigt Aufgabe 14]
	Sei $\gamma\colon[0,2\pi]\to\mathbb C$, $t\mapsto e^{it}=\cos(t)+i\sin(t)$. Skizziere $\gamma$ und berechne folgende
	Integrale:
	\begin{enumerate}
		\item $\int_\gamma z\,dz$,
		\item $\int_\gamma\frac 1z\,dz$.
	\end{enumerate}
\end{aufgabe}

\begin{center}\large\textsc{Stammfunktionen}\end{center}

Sei $G\subset\mathbb C$ ein \emph{Gebiet}, also eine zusammenhängende offene Teilmenge von $\mathbb C$. Sei $f\colon G\to\mathbb
C$ eine Funktion. Eine Funktion $F\colon G\to\mathbb C$ heißt \emph{Stammfunktion} von $f$, wenn sie holomorph ist und $F'=f$
auf ganz $G$ gilt. Stammfunktionen von Potenzreihen lassen sich mit Aufgabe 9 bestimmen.

\begin{aufgabe}[Integration mit Hilfe von Stammfunktionen]
	Zeige folgende Aussagen:
	\begin{enumerate}
		\item Wenn $f\colon G\to\mathbb C$ die Stammfunktion $F$ hat, dann gilt für jede Kurve $\gamma\colon[a,b]\to G$:
			\[
				\int_\gamma f(z)\,dz=F(z_1)-F(z_0),
			\]
			wobei $z_0=\gamma(a)$ und $z_1=\gamma(b)$ ist.

			Insbesondere hängt das Integral also nicht von dem konkreten Weg $\gamma$ ab, solange die Endpunkte festbleiben.

			\emph{Tipp: Berechne zuerst die Ableitung von $F\circ\gamma$ auf $[a,b]$. Verwende dann die Definition des Kurvenintegrals
				und den Hauptsatz der Differential- und Integralrechnung.}
		\item Wenn $\gamma$ eine geschlossene Kurve in $G$ ist (also $\gamma(a)=\gamma(b)$, und $f$ auf $G$ eine Stammfunktion hat,
			dann ist $\int_\gamma f(z)\,dz=0$.
	\end{enumerate}
\end{aufgabe}

Es gilt auch die Umkehrung von Aufgabe 16 b:

\begin{aufgabe}[Kriterium für die Existenz von Stammfunktionen -- verwendet Aufgabe 16]
	Sei $f\colon G\to\mathbb C$ stetig, und es gelte
	\[
		\int_\gamma f(z)\,dz=0
	\]
	für alle geschlossenen Kurven $\gamma$ in $G$. Dann hat $f$ eine Stammfunktion $F$ auf $G$.
	\begin{enumerate}
		\item Wir wollen uns zuerst überlegen, welche Form $F$ haben muss. Dazu nehmen wir an, dass $F(z_0)=0$ für ein $z_0\in G$ ist
			(das ist keine Einschränkung, denn die Ableitung verändert sich nicht, wenn man Konstanten hinzuzählt). Sei jetzt $\gamma$
			eine Kurve in $G$ von $z_0$ nach $z_1\in G$. Zeige, dass dann $F(z_1)=\int_\gamma f(z)\,dz$ gelten muss.
		\item Überlege dir, dass $\int_\gamma f(z)\,dz$ nur von den Endpunkten $\gamma(a)$ und $\gamma(b)$, aber nicht von dem
			konkreten Weg $\gamma$ abhängt. Wir können also $F(\zeta):=\int_{\gamma_\zeta}f(z)\,dz$ setzen, wobei $\gamma_\zeta$ eine 
			beliebige Kurve von $z_0$ nach $\zeta$ in $G$ ist.
		\item Sei jetzt $\zeta\in G$. Wir müssen zeigen, dass $F$ bei $\zeta$ komplex differenzierbar mit Ableitung $f(\zeta)$ ist. 
			Da $G$ offen 
			ist, gibt es ein $\epsilon>0$, sodass alle komplexen Zahlen $\zeta'$ mit $|\zeta-\zeta'|<\epsilon$ in $G$ liegen. Nach Teil 
			b) können
			wir als Kurve von $z_0$ nach $\zeta'$ die Hintereinanderschaltung der Kurve $\gamma_\zeta$ und des Intervalls 
			$[\zeta,\zeta']$ nehmen: Es ist also
			\[
				F(\zeta')-F(\zeta)=\int_{\gamma_\zeta}f(z)\,dz+\int_{[\zeta,\zeta']}f(z)\,dz-\int_{\gamma_\zeta}f(z)\,dz
				=\int_{[\zeta,\zeta']}f(z)\,dz.
			\]
			Verwende das, um die Definition der komplexen Ableitung für $F$ zu beweisen.
	\end{enumerate}
\end{aufgabe}

\end{document}
