\documentclass{zusammenfassung}
\usepackage{array}
\usepackage{booktabs}
\usepackage{color}
\usepackage{colortbl}
\graphicspath{ {./illustrationen/} }

\begin{document}
\maketitle{Klasse 11/12}{3. Februar 2015}{2014/2015}

\begin{center}\large\textsc{Zahlen}\end{center}

Für den Anfang: $\emptyset=\{\}$ ist die \emph{leere Menge}, die kein Element enthält. 

Konstruktion von Zahlen: Eine Zahl ist ein Objekt der Form $\{L\mid R\}$, wobei $L$ und $R$ beide Mengen von Zahlen sind und kein
Element von $L$ $\geq$ einem Element von $R$ ist. Um das zu verstehen, muss noch erklärt werden, was $\leq$ und $\geq$ bedeuten
soll. Trotzdem kennen wir jetzt schon eine Zahl:

\begin{aufgabe}[Geburtsstunde der Null]
	Überlege dir, dass $0:=\{\emptyset\mid\emptyset\}$ eine Zahl ist.
\end{aufgabe}

Für $x=\{L\mid R\}$ bezeichnen wir mit $x^L$ ein beliebiges Element von $L$, und mit $x^R$ ein beliebiges Element von $R$. 
Wir schreiben kurz $x=\{a,b,c,\ldots\mid d,e,f,\ldots\}$, wenn $L=\{a,b,c,\ldots\}$ und $R=\{d,e,f,\ldots\}$ ist. Beispielsweise
ist also $0=\{\mid\}$. Es soll folgendes Lexikon gelten:

\begin{center}
\begin{tabular}{ >{$}c<{$} l}
	\toprule
	\text{Zeichen}&Bedeutung\\
	\midrule
	x\geq y&kein $x^R\leq y$ und kein $y^L\geq x$\\
	x\leq y&$y\geq x$\\
	x\not\leq y&die Aussage $x\leq y$ gilt nicht\\
	x=y&$x\leq y$ und $y\leq x$\\
	x>y&$x\geq y$ und $y\not\geq x$\\
	x<y&$y>x$\\
	\bottomrule
\end{tabular}
\end{center}

Hiermit kann man schon so einiges anfangen:

\begin{aufgabe}[Weitere Zahlen]\label{weitere-zahlen}
	Überlege dir, dass $1:=\{0\mid\}$ und $-1:=\{\mid 0\}$ Zahlen sind. 
\end{aufgabe}

\begin{aufgabe}[Ein bisschen Ordnung -- benötigt Aufgabe \ref{weitere-zahlen}]\label{ein-bisschen-ordnung}
	Zeige folgende Aussagen:
	\begin{enumerate}
		\item $0\geq 0$, $0\leq 0$ und $0=0$,
		\item $0\not\geq 1$, $1\geq 0$ und $1>0$,
		\item $-1<0$,
		\item $-1<1$.
	\end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Keine weitere Zahl -- benötigt Aufgabe \ref{ein-bisschen-ordnung}]
  Warum ist $\{0\mid 0\}$ \emph{keine} Zahl?
\end{aufgabe}

Zwei Zahlen $x$ und $y$, die gleich sind (also $x=y$), können trotzdem sehr unterschiedlich aussehen. Wir verwenden die Notation
$x\equiv y$, wenn die linken und rechten Mengen von $x$ und $y$ übereinstimmen. Man kann natürlich annehmen, dass aus $x\equiv y$
auch $x=y$ folgt. Das ist wahr und wird in Aufgabe \ref{mehr-ordnung} bewiesen.

Man kann Zahlen auch addieren und subtrahieren. Dazu gibt es das nächste Lexikon:

\begin{center}
\begin{tabular}{ >{$}c<{$} >{$}l<{$}}
	\toprule
	\text{Zeichen}&\text{Bedeutung}\\
	\midrule
	x+y&\{x^L+y,x+y^L\mid x^R+y,x+y^R\}\\
	-x&\{-x^R\mid -x^L\}\\
	x-y&x+(-y)\\
	\bottomrule
\end{tabular}
\end{center}

\begin{aufgabe}[Wir lernen rechnen]
	Überprüfe folgende Aussagen:
	\begin{enumerate}
		\item $0+0\equiv 0$.
		\item $0+1\equiv 1$.
		\item $-0\equiv 0$.
		\item Berechne $-(1)$ und vergleiche das Ergebnis mit $-1$.
		\item Berechne $0-1$.
	\end{enumerate}
\end{aufgabe}

\begin{aufgabe}[Gleiches muss nicht gleich aussehen -- benötigt Aufgabe \ref{ein-bisschen-ordnung}]
	\label{gleiches-muss-nicht-gleich-aussehen}
	Berechne $1-1$. Ist $1-1\equiv 0$? Ist $1-1=0$?
\end{aufgabe}

\begin{aufgabe}[Sehr viele weitere Zahlen]
  Jetzt berechne $1+1$, $(1+1)+1$ und so weiter, bis du keine Lust mehr hast. Die Ergebnisse nennen wir $2$, $3$ und so weiter.
\end{aufgabe}
\pagebreak

\begin{center}\large\textsc{Spiele}\end{center}

Ein \emph{Spiel} ist eine Verallgemeinerung einer Zahl. Den Zusammenhang zu echten Spielen werden wir später kennenlernen.

Konstruktion von Spielen: Wenn $L$ und $R$ zwei Mengen von Spielen sind, dann gibt es ein Spiel $\{L\mid R\}$. Alle Spiele
entstehen auf diese Art. Zahlen sind also Spezialfälle von Spielen, da hier noch eine zusätzliche Bedingung an $L$ und $R$
gestellt wird. Die Zeichen $\leq$, $\geq$, $\not\leq$, $=$, $>$ $<$, $-$ und $+$ werden genau wie für Zahlen definiert.

Der Satz "`alle Spiele entstehen auf diese Art"' soll folgendes implizieren: Wir nehmen an, dass $P$ eine Eigenschaft ist, die 
ein Spiel haben oder nicht haben kann. Wenn wir zeigen wollen, dass alle Spiele die Eigenschaft $P$ haben, dann genügt es zu
zeigen, dass ein Spiel $x=\{L\mid R\}$ die Eigenschaft $P$ hat, falls alle Spiele aus $L$ und alle Spiele aus $R$ die Eigenschaft
$P$ haben. Wenn wir etwas über Paare $(x,y)$ von Spielen beweisen wollen, dann können wir entsprechend davon ausgehen, dass das,
was wir beweisen wollen, schon für alle Paare $(x^L,y)$, $(x^R,y)$, $(x,y^L)$ und $(x,y^R)$ wahr ist.

\begin{aufgabe}[Mehr Ordnung]\label{mehr-ordnung}
	Zeige, dass für alle Spiele $x$ gilt:
	\begin{enumerate}
		\item $x^R\not\leq x$,
		\item $x\not\leq x^L$,
		\item $x\geq x$,
		\item $x=x$.
	\end{enumerate}
	\emph{Tipp: Löse alle Aufgaben gleichzeitig. Das heißt, du kannst annehmen, dass die vier Aussagen für $x^L$ und $x^R$ bereits
		wahr sind, wenn du sie für $x$ beweist.}
\end{aufgabe}

\begin{aufgabe}[Noch mehr Ordnung]\label{noch-mehr-ordnung}
	Zeige, dass für alle Spiele $x,y,z$ mit $x\geq y$ und $y\geq z$ gilt, dass $x\geq z$. 

	\emph{Tipp: Auch hier musst du annehmen, dass die Aussage für $x^R$ anstelle von $x$ beziehungsweise für $z^L$ anstelle von $z$ 
		wahr ist.}
\end{aufgabe}

\begin{aufgabe}[Gleichheit als Äquivalenzrelation -- benötigt Aufgabe \ref{noch-mehr-ordnung}]
	Zeige folgende Aussagen für alle Spiele:
	\begin{enumerate}
		\item Aus $x=y$ folgt $y=x$,
		\item Aus $x=y$ und $y=z$ folgt $z=x$.
	\end{enumerate}
\end{aufgabe}
\pagebreak

\begin{aufgabe}[Spiele sind gut, Zahlen sind besser -- benötigt Aufgaben \ref{mehr-ordnung} und \ref{noch-mehr-ordnung}]
	\label{spiele-sind-gut-zahlen-sind-besser}
	Zeige folgende Behauptungen: 
	\begin{enumerate}
		\item Für jede Zahl $x$ gilt $x^L<x<x^R$.
		\item Für zwei beliebige Zahlen $x$ und $y$ gilt entweder $x\leq y$ oder $x\geq y$.
		\item Zeige, dass für alle Zahlen $x,y,z$ aus $x>y$ und $y\geq z$ stets $x>z$ folgt. Zeige, dass auch aus $x\geq y$ und $y>z$
			stets $x>z$ folgt.
	\end{enumerate}
\end{aufgabe}

\begin{center}\large\textsc{Die Zahlen werden geboren}\end{center}

Jede Zahl hat einen \emph{Geburtstag}, an dem sie entsteht. Die Zahl $0$ beispielsweise ist bereits am Tag $0$ entstanden. Die
Zahlen $1$ und $-1$ benötigen in ihrer Definition die Zahl $0$, sind also einen Tag später geboren worden, nämlich am Tag $1$. Die
formale Definition eines Geburtstags ist die folgende:

Der \emph{Geburtstag} $b(x)$ eines Spiels $x$ ist die Zahl $b(x):=\{b(x^L),b(x^R)\mid\}$.

\begin{aufgabe}[Geburtstagsfeier -- benötigt Aufgabe \ref{gleiches-muss-nicht-gleich-aussehen}]
	Überzeuge dich davon, dass folgende Geburtsdaten stimmen:
	\begin{enumerate}
		\item $b(0)=0$,
		\item $b(1)=1$,
		\item $b(-1)=1$,
		\item $b(1-1)=\{1\mid\}=:2$.
	\end{enumerate}
	Warum ist das kein Widerspruch dazu, dass $1-1=0$ ist?
\end{aufgabe}

\begin{aufgabe}[Sinn und Unsinn von Geburtstagen]\label{sinn-und-unsinn-von-geburtstagen}
	Warum ist der Geburtstag eines Spiels immer eine Zahl?
\end{aufgabe}

\begin{aufgabe}[Geburtstagskalender -- benötigt Aufgabe \ref{ein-bisschen-ordnung}]\label{geburtstagskalender}
	Welche Spiele wurden an den Tagen $0$, $1$, $2$ und $3$ geboren? Welche davon sind Zahlen? 
\end{aufgabe}

\begin{aufgabe}[Nachgefragt -- benötigt Aufgaben \ref{spiele-sind-gut-zahlen-sind-besser}, \ref{sinn-und-unsinn-von-geburtstagen}
	und \ref{geburtstagskalender}]
	Warum hast du bei deiner Lösung von Aufgabe \ref{geburtstagskalender} wirklich alle Spiele gefunden, die an den entsprechenden
	Tagen geboren wurden?

	\emph{Tipp: Zeige zunächst, dass $b(x)>b(x^L)$ und $b(x)>b(x^R)$ ist. Das kannst du verwenden, um zu zeigen, dass zum Beispiel
		$b(x)>0$ ist, wenn $x\not\equiv 0$ ist.}
\end{aufgabe}

\end{document}
