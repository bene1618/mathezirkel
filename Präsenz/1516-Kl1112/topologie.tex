\documentclass{zusammenfassung}
\usepackage{array}
\usepackage{booktabs}
\usepackage{color}
\usepackage{colortbl}
\graphicspath{ {./illustrationen/} }

\begin{document}
\maketitle{Klasse 11/12}{03. Juni 2016}{2015/2016}

\begin{center}\large\textsc{Topologische Räume}\end{center}

Hier bezeichnet $X$ immer eine Menge. Eine \emph{Familie von Umgebungen} für $X$ besteht für jeden Punkt $p\in X$ aus einer Menge
$\mathcal U_p$ von Teilmengen von $X$ (die Elemente von $\mathcal U_p$ heißen \emph{Umgebungen} von $p$), sodass folgende 
Eigenschaften für jeden Punkt $p\in X$ erfüllt sind:
\begin{itemize}
  \item[Ü1)] Für alle Umgebungen $U\in\mathcal U_p$ von $p$ ist $p\in U$,
  \item[Ü2)] Die ganze Menge $X$ ist eine Umgebung von $p$, also $X\in\mathcal U_p$,
  \item[Ü3)] Wenn $U\in\mathcal U_p$ eine Umgebung von $p$ ist und $U\subset V\subset X$ gilt, dann ist auch $V$ eine Umgebung von
    $p$, also $V\in\mathcal U_p$.
  \item[Ü4)] Wenn $U,V\in\mathcal U_p$ Umgebungen von $p$ sind, dann ist auch $U\cap V\in\mathcal U_p$ eine Umgebung von $p$.
\end{itemize}
Ein \emph{topologischer Raum} ist eine Menge $X$ zusammen mit einer Familie von Umgebungen wie oben.

Jetzt lassen sich weitere Begriffe definieren: Dazu betrachten wir eine Teilmenge $S\subset X$. Ein Punkt $p\in X$ heißt
\emph{Randpunkt} von $S$, wenn $U\cap S\neq\emptyset$ und $U\cap(X\setminus S)\neq\emptyset$ für alle Umgebungen $U\in\mathcal
U_p$ von $p$ gilt, wenn also jede Umgebung von $p$ sowohl $S$ als auch das Komplement von $S$ schneidet. Der Menge der Randpunkte
von $S$ bezeichnen wir mit $\partial S$, dem \emph{Rand} von $S$.

Eine Teilmenge $S\subset X$ heißt \emph{offen}, wenn ihr Rand leer ist. Sie heißt \emph{abgeschlossen}, wenn sie ihren Rand
enthält. $S$ ist also genau dann offen, wenn $\partial S=\emptyset$ gilt, und genau dann abgeschlossen, wenn $\partial S\subset S$
ist.

Der \emph{Abschluss} einer Menge $S\subset X$ ist $\overline S=S\cup\partial S$, und das \emph{Innere} von $S$ ist
$S^\circ=S\setminus\partial S$.

\begin{aufgabe}[Charakterisierung der offenen Teilmengen]\label{Charakterisierung der offenen Teilmengen}
  Beweise: Eine Teilmenge $S\subset X$ ist genau dann offen, wenn $S$ eine Umgebung von jedem Punkt $p\in S$ ist.
\end{aufgabe}

\begin{aufgabe}[Axiome für offene Teilmengen]\label{Axiome für offene Teilmengen}
  Die offenen Teilmengen von $X$ erfüllen folgende Eigenschaften:
  \begin{itemize}
    \item[O1)] $\emptyset$ und $X$ sind offen,
    \item[O2)] Wenn $I$ eine Menge und $U_i$ für alle $i\in I$ offene Teilmengen von $X$ sind, dann ist auch
      \[
        \bigcup_{i\in I}U_i=\{x\in X:\exists i\in I\colon x\in U_i\}
      \]
      eine offene Teilmenge von $X$.
    \item[O3)] Wenn $U,V\subset X$ offene Teilmengen von $X$ sind, dann ist auch $U\cap V$ eine offene Teilmenge von $X$.
  \end{itemize}
\end{aufgabe}

\begin{aufgabe}[Charakterisierung der abgeschlossenen Teilmengen]\label{Charakterisierung der abgeschlossenen Teilmengen}
  Beweise: Eine Teilmenge $S\subset X$ ist genau dann abgeschlossen in $X$, wenn ihr Komplement $X\setminus S$ offen in $X$ ist.
\end{aufgabe}

\begin{aufgabe}[Axiome für abgeschlossene Teilmengen]\label{Axiome für abgeschlossene Teilmengen}
  Die abgeschlossenen Teilmengen von $X$ erfüllen folgende Eigenschaften:
  \begin{itemize}
    \item[A1)] $\emptyset$ und $X$ sind abgeschlossen,
    \item[A2)] Wenn $I$ eine Menge und $U_i$ für alle $i\in I$ abgeschlossene Teilmengen von $X$ sind, dann ist auch
      \[
        \bigcap_{i\in I}U_i=\{x\in X:\forall i\in I\colon x\in U_i\}
      \]
      eine abgeschlossene Teilmenge von $X$.
    \item[A3)] Wenn $U,V\subset X$ abgeschlossene Teilmengen von $X$ sind, dann ist auch $U\cup V$ eine abgeschlossene Teilmenge 
      von $X$.
  \end{itemize}
\end{aufgabe}

\begin{aufgabe}[Charakterisierung von Umgebungen]\label{Charakterisierung von Umgebungen}
  Beweise: Eine Menge $S\subset X$ ist genau dann eine Umgebung von $p\in X$, wenn eine offene Menge $U\subset X$ mit $p\in
  U\subset S$ existiert.
\end{aufgabe}

\begin{aufgabe}[Alternative Definition eines topologischen Raums]\label{Alternative Definition eines topologischen Raums}
  Zeige, dass die Axiome aus Aufgabe \ref{Axiome für offene Teilmengen} auch für die Definition eines topologischen Raums
  verwendet werden hätten können: Ein topologischer Raum besteht dann also aus einer Menge $X$ und einer Menge $\mathcal O_X$
  von Teilmengen von $X$, den \emph{offenen Teilmengen} von $X$. Dabei sollen die Axiome O1 bis O3 gelten. Umgebungen sind dann
  wie in Aufgabe \ref{Charakterisierung von Umgebungen} definiert. Zeige, dass dann die Axiome Ü1 bis Ü4 erfüllt sind.
\end{aufgabe}

\begin{aufgabe}[Charakterisierung des Abschlusses]\label{Charakterisierung des Abschlusses}
  Zeige, dass $\overline S$ die kleinste abgeschlossene Teilmenge von $X$ ist, die $S$ enthält. Dazu muss man folgendes beweisen:
  \begin{itemize}
    \item $\overline S$ ist abgeschlossen und
    \item wenn $V\subset X$ eine abgeschlossene Teilmenge von $X$ mit $S\subset V$ ist, dann ist auch $\overline S\subset V$.
  \end{itemize}
\end{aufgabe}

\begin{aufgabe}[Charakterisierung des Inneren]\label{Charakterisierung des Inneren}
  Zeige, dass $S^\circ$ die kleinste offene Teilmenge von $X$ ist, die in $S$ enthalten ist. Dazu muss man folgendes beweisen:
  \begin{itemize}
    \item $S^\circ$ ist offen und
    \item wenn $V\subset X$ eine offene Teilmenge von $X$ mit $V\subset S$ ist, dann ist auch $V\subset S^\circ$.
  \end{itemize}
  Zeige, dass genau dann $p\in S^\circ$ gilt, wenn $S\in\mathcal U_p$ ist, also wenn $S$ eine Umgebung von $p$ ist.
\end{aufgabe}

\begin{aufgabe}[Charakterisierung des Randes]\label{Charakterisierung des Randes}
  Zeige: Es ist $\partial S=\overline S\setminus S^\circ$.
\end{aufgabe}

\begin{center}\large\textsc{Stetige Abbildungen}\end{center}

Es seien $X$ und $Y$ topologische Räume und $f\colon X\to Y$ eine Abbildung (also eine Zuordnung, die \emph{jedem} Element $x\in
X$ \emph{genau ein} Element $f(x)\in Y$ zuordnet). Wir betrachten einen Punkt $p\in X$. Die Abbildung $f$ heißt \emph{stetig am
  Punkt $p$}, wenn für jede Umgebung $V$ von $f(p)$ eine Umgebung $U$ von $p$ existiert, sodass $f(U)\subset V$ gilt, also 
$f(q)\in V$ für alle Punkte $q\in U$.

Für eine Teilmenge $S\subset Y$ ist $f^{-1}(S):=\{x\in X:f(x)\in S\}$ das \emph{Urbild} von $S$ unter $f$.

\begin{aufgabe}[Charakterisierung von stetigen Abbildung mittels offener Mengen]\label{Charakterisierung von stetigen Abbildung
    mittels offener Mengen}
  Zeige: Eine Abbildung $f\colon X\to Y$ zwischen topologischen Räumen ist genau dann stetig, wenn für jede in $Y$ offene Teilmenge
  $U\subset Y$ gilt, dass das Urbild $f^{-1}(U)\subset X$ offen in $X$ ist.
\end{aufgabe}

\end{document}
