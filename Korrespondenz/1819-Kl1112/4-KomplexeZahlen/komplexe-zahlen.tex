\documentclass{korrespondenz}
\graphicspath{ {../../illustrationen/} }

\usepackage{enumitem}

\newcommand{\N}{{\mathbb N}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\Q}{{\mathbb Q}}
\newcommand{\R}{{\mathbb R}}
\newcommand{\C}{{\mathbb C}}

\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Erinnere dich kurz an die Zeit, als du rechnen gelernt hast. Zuerst hast du höchstwahrscheinlich das Zählen gelernt: eins, zwei,
drei, \dots Am Anfang der Grundschulzeit wurde dann die Addition eingeführt. Du konntest jetzt zwei (oder mehrere Zahlen)
addieren. Der nächste Schritt war die Subtraktion. Die hast du höchstwahrscheinlich in irgendeiner Form als Umkehrproblem zur
Addition kennengelernt. Dabei konnte man immer nur eine kleinere Zahl von einer größeren abziehen ("`wegnehmen"'). Aufgaben der
Form $3-7=?$ waren zunächst nicht lösbar (und ergaben wohl auch keinen Sinn). Ähnlich verhielt es sich mit der Division: Die
Division zweier Zahlen war teilweise nur mit Rest möglich.

Wir springen ein paar Jahre weiter: Du hast gelernt, dass man doch beliebige Zahlen voneinander abziehen kann, wenn man
\emph{negative Zahlen} einführt. Genauso hast du gelernt, (fast) beliebige Zahlen durcheinander zu teilen, wenn man \emph{Brüche}
einführt. Außerdem hast du einige Rechenregeln kennengelernt, nämlich:
\begin{itemize}
  \item die \emph{Kommutativgesetze der Addition und der Multiplikation}:
    \begin{align*}
      x+y&=y+x,\\
      x\cdot y&=y\cdot x
    \end{align*}
    für alle Zahlen $x$ und $y$,
  \item die \emph{Assoziativgesetze der Addition und der Multiplikation}:
    \begin{align*}
      x+(y+z)&=(x+y)+z,\\
      x\cdot(y\cdot z)&=(x\cdot y)\cdot z
    \end{align*}
    für alle Zahlen $x$, $y$ und $z$, sowie
  \item das \emph{Distributivgesetz}:
    \[
      x\cdot(y+z)=x\cdot y+x\cdot z
    \]
    für alle Zahlen $x$, $y$ und $z$.
\end{itemize}
Vielleicht ist dir aufgefallen, dass ich das zweite Distributivgesetz
\[
  (x+y)\cdot z=x\cdot z+y\cdot z
\]
nicht gesondert aufgeführt habe.

\begin{aufgabe}
  Zeige, wie das zweite Distributivgesetz aus dem ersten und dem Kommutativgesetz der Multiplikation folgt.
\end{aufgabe}

Weiterhin hast du besondere Zahlen $0$ und $1$ kennengelernt, die die Eigenschaften
\begin{align*}
  0+x&=x,\\
  1\cdot x&=x
\end{align*}
für alle Zahlen $x$ erfüllen.

Zusammengefasst ist also folgendes passiert: Zuerst waren Zahlen \emph{natürliche Zahlen} $\N=\{0,1,2,\ldots\}$ (eventuell auch erst
einmal ohne Null, aber lassen wir das beiseite). Zwei Zahlen $x,y\in\N$ konnte man addieren, also $x+y\in\N$ berechnen. Umgekehrt
konnte man die Aufgabe $a+x=b$ nach $x$ auflösen, falls $a\leq b$ ist. Das führt zur Subtraktion $x=b-a\in\N$. Nun wollte man etwa
die Gleichung $5+x=4$ lösen. Dazu wurde der Zahlenbereich erweitert zu den \emph{ganzen Zahlen}
$\Z=\{\ldots,-2,-1,0,1,2,\ldots\}$. In $\Z$ kann man immer noch addieren und multiplizieren, und hier können nun allle Gleichungen 
der Form $a+x=b$ gelöst werden. Konkret gibt es \emph{additive Inverse}, also zu jeder Zahl $x\in\Z$ eine Zahl $-x\in\Z$, sodass
\[
  x+(-x)=0
\]
ist. Man kürzt nun $y-x=y+(-x)$ ab.

\begin{aufgabe}\label{aufg:2}
  Überlege dir, dass aus der Existenz von additiven Inversen und den Rechengesetzen folgt:
  \begin{enumerate}
    \item falls $x+x=x$ gilt, ist $x=0$,
    \item für alle Zahlen $x$ gilt $0\cdot x=0$. \label{item:b}
  \end{enumerate}
\end{aufgabe}

Genauso will man Gleichungen wie $3\cdot x=5$ lösen. Das führt zu den \emph{rationalen Zahlen} $\Q=\left\{\frac
zn:z\in\Z,n\in\Z-\{0\}\right\}$. Natürlich kann man jetzt alle Gleichungen der Form $a\cdot x=b$ nach $x$ auflösen, außer $a=0$:
denn nach Aufgabe \ref{aufg:2} \ref{item:b} ist ja $0\cdot x$ immer gleich $0$.

Trotzdem kann man immer noch einige Gleichungen nicht lösen. Beispielsweise gibt es keine rationale Zahl $x=\frac zn\in\Q$, deren
Quadrat gleich $2$ ist. Wäre nämlich $x^2=2$, dann würde $z^2=2n^2$ folgen. Wir zerlegen beide Seiten der Gleichung in
Primfaktoren. Da die Primfaktorzerlegung eindeutig ist, müssen beide Seiten der Gleichung die gleiche Anzahl an Primfaktoren
besitzen. Allerdings hat $z^2$ eine gerade Anzahl an Primfaktoren (doppelt so viele wie $z$), und $2n^2$ hat eine ungerade Anzahl
an Primfaktoren (doppelt so viele wie $z$ plus eins). Das kann nicht sein, also muss die Annahme $x^2=2$ falsch sein.

Umgekehrt ist es aber so, dass man $x\in\Q$ finden kann, sodass $x^2$ beliebig nahe an $2$ liegt. Genauer: für jede Zahl
$\epsilon\in\Q$ mit $\epsilon>0$ (sehr kleine Zahlen werden in der Mathematik typischerweise $\epsilon$ genannt) findet man eine 
rationale Zahl $x\in\Q$ (also einen Bruch), sodass $2-\epsilon<x^2<2$.

\begin{aufgabe}\label{aufg:3}
  Beweise das, indem du die folgenden Aufgaben löst:
  \begin{enumerate}
    \item es gibt eine Zahl $n\in\N$ mit der Eigenschaft, dass $\frac 1n<\epsilon$ ist. \emph{Hinweis: Schreibe zuerst $\epsilon$
      als Bruch}.
    \item es gibt eine Zahl $z\in\N$ mit der Eigenschaft, dass $(\frac zn)^2<2$ und $(\frac{z+1}n)^2>2$ ist.
    \item die Zahl $x=\frac zn$ erfüllt $2-\epsilon<x^2$.
  \end{enumerate}
\end{aufgabe}

Dieses Beispiel zeigt, dass es bei den rationalen Zahlen $\Q$ noch "`Lücken"' in der Zahlengerade gibt. Füllt man diese Lücken
auf, dann erhält man die Menge der \emph{reellen Zahlen} $\R$, die man sich als die vollständige Zahlengerade vorstellen kann.
Tatsächlich heißt die wesentliche Eigenschaft, die $\R$ von $\Q$ unterscheidet, \emph{Vollständigkeit}. Damit wollen wir uns aber
heute nicht beschäftigen.

Obwohl die Zahlengerade jetzt keine Lücken mehr hat, können wir doch noch nicht beliebige Gleichungen lösen. Offensichtlich
funktioniert das nicht bei Gleichungen wie $0\cdot x=1$, aber auch nicht bei der Gleichung $x^2=-1$. Der Grund dafür ist, dass
entweder $x\geq 0$ oder $x\leq 0$ gelten muss. Das Produkt zweier positiver Zahlen ist aber positiv, und das Produkt zweier
negativer Zahlen ist ebenfalls positiv, sodass in jedem Fall $x^2\geq 1$ sein muss. Das Problem hierbei ist also, dass die reellen
Zahlen \emph{geordnet} sind: es gibt also eine \emph{Relation} $<$ (bzw. $\leq$, $>$, $\geq$), die bestimmte Rechenregeln erfüllt.
Manche davon hast du wahrscheinlich auch bei der Lösung von Aufgabe \ref{aufg:3} benutzt. Wollen wir also die Gleichung $x^2=-1$
lösen, dann müssen wir auf diese Anordnung verzichten.

Die Funktion $x\mapsto x^2$, die jeder Zahl ihr Quadrat zuordnet, kann man auf der Zahlengerade folgendermaßen darstellen:
\begin{center}
  \begin{tikzpicture}
    \draw[->] (-5.2,0)--(9.2,0);
    \foreach\i in {-2,...,4} {\draw (2*\i,-0.1)--(2*\i,0.1) node [above=0.3] {$\i$};}
    \draw[|->] (-1,-0.2) arc (-180:0:0.75);
    \draw[|->] (-2,-0.2) arc (-180:0:2);
    \draw[|->] (-3,-0.2) arc (-180:0:3.75);
    \draw[|->] (-4,-0.2) arc (-180:0:6);
    \draw[|->] (1,0.2)--(0.5,0.2);
    \draw[|->] (4,0.2)--(8,0.2);
  \end{tikzpicture}
\end{center}

Es fällt auf, dass der linke Teil der Zahlengerade auf den rechten Teil "`gedreht wird"', während der rechte Teil nur skaliert
wird. In beiden Fällen wird der Winkel zu der Halbgeraden $[0,+\infty[$ verdoppelt. Wir können nun folgendes machen: wenn
wir annehmen, dass Quadrieren eine Verdoppelung des Winkels zur positiven Halbgerade bedeutet, dann können wir annehmen, dass eine
Zahl, die "`oberhalb"' der $0$ liegt, auf eine negative reelle Zahl abgebildet wird:
\begin{center}
  \begin{tikzpicture}
    \draw[->] (-5.2,0)--(9.2,0);
    \foreach\i in {-2,...,4} {\draw (2*\i,-0.1)--(2*\i,0.1);}
    \draw[|->] (-1,-0.2) arc (-180:0:0.75);
    \draw[|->] (-3,-0.2) arc (-180:0:3.75);
    \draw[|->] (1,0.2)--(0.5,0.2);
    \draw[|->] (4,0.2)--(8,0.2);
    \fill (0,2) circle (2pt) node [right] {$i$};
    \draw[|->] (95:2) arc (95:175:2);
  \end{tikzpicture}
\end{center}
Diese Zahl nennen wir $i$, die \emph{imaginäre Einheit.} Es ist klar, wieso hier die Ordnung verloren geht: Für die "`Zahl"' $i$
können wir nicht sagen, ob sie positiv oder negativ ist. Wenn wir mit $i$ rechnen wollen, müssen wir noch einige weitere Zahlen
hinzufügen: Beispielsweise sollte man $i$ mit beliebigen anderen Zahlen multiplizieren können. Das heißt, dass wir beispielsweise
auch die Zahlen $3i$ oder $\pi i$ benötigen. Weiterhin wollen wir auch Zahlen addieren können, brauchen also zum Beispiel $2i+5$.
Solche Zahlen müssen nun wieder addiert und multipliziert werden können. Es stellt sich allerdings heraus, dass hier immer nur
Zahlen der Form $ai+b$ mit $a,b\in\R$ entstehen: Um das zu sehen, erinnern wir uns daran, dass ja $i^2=-1$ sein soll, und dass die
Rechengesetze wie die Assoziativ- und Kommutativgesetze sowie das Distributivgesetz erfüllt sein soll.

\begin{aufgabe}
  Rechne nach, dass wir dann folgende Regeln für die Addition und die Multiplikation haben, wenn $a,b,c,d\in\R$ beliebige reelle
  Zahlen sind:
  \begin{enumerate}
    \item $(ai+b)+(ci+d)=(a+c)i+(b+d)$ und
    \item $(ai+b)\cdot(ci+d)=(ad+bc)i+(bd-ac)$.
  \end{enumerate}
\end{aufgabe}

\begin{aufgabe}
  Rechne aus:
  \begin{enumerate}
    \item $(2i+3)\cdot(5i-4)$,
    \item $i^4$,
    \item $i^{1373}$.
  \end{enumerate}
\end{aufgabe}

Die Menge der Zahlen $ai+b$ mit $a,b\in\R$ sind die \emph{komplexen Zahlen} $\C$. Wir haben gesehen, dass man in $\C$ die
Gleichung $x^2=-1$ lösen kann, allerdings auf Kosten der Anordnung. Es gibt aber noch einige weitere Gleichungen, die jetzt eine
Lösung besitzen:

\begin{aufgabe}
  Finde Lösungen für folgende Gleichungen in $\C$:
  \begin{enumerate}
    \item $x^4+2x^2=-1$,
    \item $2x^2-3x+5=0$,
    \item $x^2=a$ für beliebige $a\in\R$.
    \item $ax^2+bx+c=0$, wobei $a,b,c\in\R$ sind und $a\neq 0$ ist. \emph{Hinweis: Du kannst die Mitternachtsformel verwenden.
        Allerdings musst du erklären, was die Wurzel bedeuten soll, wenn die Zahl unter der Wurzel negativ ist. Rechne nach, dass
      die Formel tatsächlich Lösungen für die Gleichung liefert, indem du die Lösungen einsetzt und vereinfachst.}
  \end{enumerate}
\end{aufgabe}

Tatsächlich kann man zeigen, dass \emph{jede} Gleichung der Form
\[
  a_0+a_1x+a_2x^2+\ldots+a_nx^n=0
\]
mit komplexen Koeffizienten $a_0,\ldots,a_n\in\C$, $n\geq 1$ und $a_n\neq 0$ eine Lösung $x\in\C$ besitzt. Diese Aussage wird der
\emph{Fundamentalsatz der Algebra} genannt. Diesen Satz wollen wir später auf dem Zettel noch beweisen.

Wichtig für den Beweis ist, dass Zahlen in $\C$ einen \emph{Absolutbetrag} besitzen, nämlich den Abstand von der Zahl $0$ in der 
Ebene. Es ist also
\[
  |ai+b|=\sqrt{a^2+b^2}.
\]
Es gibt noch eine weitere Operation auf den komplexen Zahlen, die auf den reellen Zahlen nicht sonderlich interessant ist, nämlich
die \emph{komplexe Konjugation}. Und zwar ist das komplex Konjugierte einer Zahl $x=ai+b$ die Zahl
\[
  \bar x=-ai+b.
\]

\begin{aufgabe}\label{aufg:7}
  Beweise:
  \begin{enumerate}
    \item Eine Zahl $x\in\C$ erfüllt $\bar x=x$ genau dann, wenn $x\in\R$ ist.
    \item Für jede Zahl $x\in\C$ ist $x\bar x\in\R$ eine reelle Zahl.
    \item Tatsächlich ist immer $|x|=\sqrt{x\bar x}$.
    \item Für eine Zahl $x\in\C$ folgt aus $|x|=0$, dass $x=0$ sein muss.\label{item:d}
    \item Für Zahlen $x,y\in\C$ gilt immer $|xy|=|x||y|$.
    \item Für Zahlen $x,y\in\C$ gilt immer $|x+y|\leq|x|+|y|$.
  \end{enumerate}
\end{aufgabe}

\begin{aufgabe}\label{aufg:8}
  Für komplexe Zahlen auf dem Einheitskreis $\{x\in\C\mid|x|=1\}$ ist die Multiplikation besonders anschaulich. Unter anderem das
  zeigt diese Aufgabe.
  \begin{enumerate}
    \item Überlege, dass du jede Zahl $x\in\C$ mit $|x|=1$ in der Form $x=\cos(\theta)+i\sin(\theta)$ mit $\theta\in\R$ schreiben 
      kannst. Was ist dabei die Bedeutung der Zahl $\theta$?
    \item Beweise
      $(\cos(\theta_1)+i\sin(\theta_1))\cdot(\cos(\theta_2)+i\sin(\theta_2))=\cos(\theta_1+\theta_2)+i\sin(\theta_1+\theta_2)$.
      \emph{Hinweis: Verwende die Additionstheoreme für Kosinus und Sinus aus der Formelsammlung.}
    \item Wir betrachten beliebige Zahlen $n\in\N$, $n\geq 1$, und $y\in\C$ mit $|y|=1$. Zeige, dass die Gleichung $x^n=y$ eine
      Lösung $x\in\C$ hat.
    \item Zeige, dass die Gleichung $x^n=y$ auch eine Lösung $x\in\C$ hat, wenn $|y|\neq 1$ ist. \emph{Hinweis: Falls $y=0$ ist,
      ist das leicht. Falls $y\neq 0$ ist, ist auch $|y|\neq 0$ und du kannst zuerst die Gleichung $x^n=\frac y{|y|}$ lösen.}
      \label{item:8d}
  \end{enumerate}
\end{aufgabe}

Wir wollen jetzt den Fundamentalsatz der Algebra beweisen. Dazu betrachten wir also die Funktion
\[
  p\colon\C\to\C,\quad p(x)=a_0+a_1x+a_2x^2+\ldots+a_nx^n
\]
und wollen eine \emph{Nullstelle} finden, also eine Zahl $x\in\C$ mit $p(x)=0$.

Wir benötigen eine Eigenschaft der komplexen Zahlen, die aus der oben erwähnten Vollständigkeit der reellen Zahlen folgt, und die
das Ziel der folgenden Aufgabe ist. 

Das Ziel der Aufgabe ist es zu beweisen, dass die Abbildung
\[
  |p|\colon\C\to\C,\quad|p|(x)=|p(x)|
\]
ein globales Minimum annimmt. Wir wollen also einen Punkt $y\in\C$ finden, sodass $|p(y)|\leq|p(x)|$ für alle Punkte
$x\in\C$ gilt. Damit sind wir dem Fundamentalsatz der Algebra ein Stück weit näher gekommen, denn wir müssen wegen Aufgabe
\ref{aufg:7} \ref{item:d} nur noch beweisen, dass $|p(y)|=0$ sein muss.

\begin{aufgabe}
  Die Aufgabe besteht aus mehreren Teilschritten. Beweise also folgende Aussagen:
  \begin{enumerate}
    \item Für jede Zahl $x\in\C$ gilt
      \[
        |p(x)|\geq|a_n||x|^n-|a_0|-|a_1||x|-|a_2||x|^2-\ldots-|a_{n-1}||x|^{n-1}.
      \]
    \item Überlege dir, warum $|a_n|\neq 0$ ist.
    \item Wir schreiben
      \[
        R=\max\left\{1,\frac{1+2|a_0|+|a_1|+\ldots+|a_{n-1}|}{|a_n|}\right\}.
      \]
      Beachte, dass $R>0$ eine wohldefinierte Zahl ist, da $|a_n|\neq 0$ ist. Zeige, dass für jede Zahl $x\in\C$ mit $|x|\geq R$ 
      gilt: $|p(x)|\geq |a_0|+1$.
    \item Finde eine Zahl $x\in\C$ mit $|p(x)|=|a_0|$.
    \item Überlege dir, dass es für jede natürliche Zahl $n\geq 1$ eine Zahl $x_n\in\C$ gibt, sodass $|p(x_n)|-\frac 1n\leq|p(x)|$ 
      für alle Zahlen $x\in\C$ gilt. Die Zahlen $x_n$ sind also Stellen, an denen die Funktion $|p|$ \emph{fast} ihr globales
      Minimum annimmt. \emph{Hinweis: Diese Aufgabe ist relativ schwer. Hier ist eine mögliche Lösung für $n=1$: Beginne mit
      irgendeiner Zahl $x\in\C$. Wenn es eine Zahl $x'\in\C$ mit $|p(x')|<|p(x)|-1$ gibt, ersetze $x$ durch $x'$. Da immer
      $|p(x)|\geq 0$ ist, findest du so irgendwann eine Zahl $x_1$ wie in der Aufgabe gefordert.}
    \item Überlege dir, dass die Zahlen $x_n$ aus dem letzten Aufgabenteil alle $|x|\leq R$ erfüllen müssen.
    \item Zeige, dass die Zahlen $x_n$ einen \emph{Häufungspunkt} besitzen: Das heißt, es gibt eine Zahl $y\in\C$, sodass für
      jede natürliche Zahl $m\geq 1$ unendlich viele der Zahlen $x_n$ innerhalb eines Kreises mit Radius $\frac 1m$ um $y$ liegen.
      Das heißt also, dass $|y-x_n|<\frac 1m$ für unendlich viele Zahlen $n$ gelten soll. Diese Aufgabe ist ebenfalls sehr
      anspruchsvoll und wird daher in Teilaufgaben gegliedert. Falls du keine Lust auf diese lange Aufgabe hast, überlege dir
      anschaulich, was ein Häufungspunkt ist und warum die Zahlen $x_n$ einen solchen haben müssen.
      \begin{enumerate}
        \item Wir vereinbaren eine Abkürzung: Und zwar bezeichnen wir für eine Zahl $r>0$ und eine komplexe Zahl $z\in\C$
          die Kreisscheibe um $z$ mit Radius $r$ mit $B_r(z)$. Es ist also
          \[
            B_r(z)=\{x\in\C\mid|x-z|<r\}.
          \]
          Überlege dir, dass es zu jeder Kreisscheibe $B_r(z)$ und jeder natürlichen Zahl $m\geq 0$ eine endliche Anzahl an 
          komplexen Zahlen gibt, sodass die Kreisscheiben mit Radius $\frac 1m$ um diese Zahlen die gesamte Kreisscheibe
          $B_r(z)$ überdecken, wie in folgender Abbildung:
          \begin{center}
            \begin{tikzpicture}
              \fill (0,0) circle (2pt) node [below right] {$z$};
              \draw (0,0) -- node[above] {$r$} (20:4);
              \draw (0,0) circle (4);
              \fill[gray] (3,2) circle (2pt);
              \draw[gray] (3,2) -- node[above] {$\frac 1m$} +(10:1.8);
              \draw[gray] (3,2) circle (1.8);
              \foreach\i/\j in {(1,2.5)/30,(0,3.8)/50,(-1,2.2)/100,(-2.3,1.6)/120,(-0.5,0.1)/115,(2.55,-0.3)/-10,(1.9,-1.9)/-40,
              (-3,-0.7)/190,(-2,-2)/260,(0.2,-3.3)/80}
              {
                \fill[gray] \i circle (2pt);
                \draw[gray] \i -- +(\j:1.8);
                \draw[gray] \i circle (1.8);
              }
            \end{tikzpicture}
          \end{center}
        \item Beginne mit der Kreisscheibe $B_R(0)$. Warum enthält diese Kreisscheibe unendlich viele der Zahlen $x_n$?
        \item Angenommen, wir wissen bereits, dass es eine Menge $B_m$ gibt, die unendlich viele der Zahlen $x_n$ enthält, und die
          in einer Kreisscheibe mit Radius $\frac 1{2^m}$ enthalten ist. Überlege dir, dass es dann eine Teilmenge 
          $B_{m+1}\subset B_m$ geben muss, die in einer Kreisscheibe mit Radius $\frac 1{2^{m+1}}$ enthalten ist und die ebenfalls 
          unendlich viele der
          Zahlen $x_n$ enthält. \emph{Hinweis: Überdecke die Kreisscheibe, in der $B_m$ enthalten ist, mit endlich vielen
            Kreisscheiben vom Radius $\frac 1{2^{m+1}}$. Da es unendlich viele Zahlen $x_n$ gibt, müssen in einer dieser kleineren
            Kreisscheibe -- nennen wir sie einmal $K$ -- auch unendlich viele der Zahlen $x_n$ liegen. Indem du nur Zahlen $x_n$
            betrachtest, die in $B_m$ enthalten sind, kannst du sehen, dass auch $B_{m+1}=K\cap B_m$ unendlich viele Zahlen $x_n$
          enthält.}
        \item Wähle jeweils eine beliebige Zahl $y_m$ aus jedem $B_m$. Beweise, dass dann $B_k\subset B_{1/2^{m-1}}(y_m)$ für jedes
          $k\geq m$ gilt. \emph{Hinweis: $B_k\subset B_m$, das heißt, du musst nur beweisen, dass $B_m\subset B_{1/2^{m-1}}(y_m)$ 
          gilt.}
        \item Überlege dir, dass $|y_m-x_n|<\frac 1{2^{m-1}}$ für unendlich viele Zahlen $n\in\N$ gilt, und dass $|y_m-y_k|<\frac
          1{2^{m-1}}$ für alle Zahlen $k\geq m$ gilt.
        \item Überlege dir anschaulich, dass es eine Zahl $y\in\C$ geben muss, die $|y_m-y|<\frac 1{2^{m-1}}$ für alle Zahlen
          $m\in\N$ erfüllt. \emph{Hinweis: Hier wird die Vollständigkeit von $\R$ (bzw. von $\C$) verwendet, also die Tatsache,
          dass $\C$ keine Lücken enthält.}
        \item Begründe, warum $y$ ein Häufungspunkt ist.
      \end{enumerate}
    \item Überlege dir, dass an diesem Häufungspunkt $y$ die Funktion $|p|$ ein globales Minimum hat, also dass $|p(y)|\leq|p(x)|$
      für alle $x\in\C$ gilt.
  \end{enumerate}
\end{aufgabe}

Puh, geschafft! Das war der schwerste Teil des Arguments. Wir wissen jetzt also, dass ein Punkt $y\in\C$ existiert, an dem $|p|$
ein globales Minimum annimmt. Als nächstes zeigen wir, dass wir genauso gut annehmen können, dass $y=0$ ist, indem wir ein anderes
Polynom $q$ betrachten. 

\begin{aufgabe}
  Wir betrachten also die folgende Funktion:
  \[
    q(x)=p(x-y)=a_0+a_1(x-y)+a_2(x-y)^2+\ldots+a_n(x-y)^n.
  \]
  \begin{enumerate}
    \item Beweise, dass die Funktion $q$ ebenfalls ein \emph{Polynom $n$-ten Grades in $x$} ist. Das heißt, es gibt Zahlen 
      $b_0,\ldots,b_n$ (die von $y$ abhängen dürfen!), sodass
      \[
        q(x)=b_0+b_1x+b_2x^2+\ldots+b_nx^n
      \]
      für alle $x\in\C$ gilt. \emph{Hinweis: Überlege dir die Aussage zunächst für den Spezialfall $p(x)=x^n$.}
    \item Beweise, dass $|q(x)|$ ein Minimum bei $x=0$ annimmt.
    \item Überlege dir, dass $p$ genau dann eine Nullstelle hat, wenn $|q(0)|=0$ ist.
  \end{enumerate}
\end{aufgabe}

Wir nehmen jetzt für einen Widerspruchsbeweis an, dass $p$ keine Nullstelle hat, also dass $|q(0)|>0$ ist. Wir schreiben
$M=|q(0)|>0$. Unser Ziel ist es zu beweisen, dass es in diesem Fall eine Zahl $z\in\C$ mit $|q(z)|<M$ gibt. Das kann aber nicht
sein, weil ja $|q(0)|$ minimal ist. Also folgt, dass $|q(0)|=0$ gewesen sein muss.

Indem wir Summanden weglassen, wenn sie gleich $0$ sind, können wir $q$ schreiben als
\[
  q(x)=b_0+b_kx^k+b_{k+1}x^{k+1}+\ldots+b_nx^n,
\]
wobei $b_k\neq 0$ ist. 

\begin{aufgabe}
  Der Abschluss des Beweises des Fundamentalsatzes der Algebra ist diese Aufgabe.
  \begin{enumerate}
    \item Warum ist auch $b_0\neq 0$?
    \item Begründe, dass es eine Zahl $x_0\in\C$ mit $x_0^k=-\frac{b_0}{b_k}$ gibt.
    \item Rechne nach, dass für jede Zahl $r>0$ gilt:
      \[
        q(rx_0)=b_0-r^kb_0+\underbrace{b_{k+1}r^{k+1}x_0^{k+1}+\ldots+b_nr^nx_0^n}_{=E}=b_0(1-r^k)+E.
      \]
    \item Wir schreiben $s=|q_0/q_m|^{1/m}$. Überlege dir, dass $|x_0|=s$ ist. \emph{Hinweis: Beweise zuerst, dass $|x_0|^m=s^m$
      ist.}
    \item Wir schreiben $M=\max\{|b_{k+1}|,\ldots,|b_n|\}$. Rechne nach, dass für den Fehlerterm 
      $E=b_{k+1}r^{k+1}x_0^{k+1}+\cdots+b_nr^nx_0^n$ gilt:
      \[
        |E|\leq M\cdot(rs)^{k+1}\cdot(1+(rs)+\ldots+(rs)^{n-k-1}).
      \]
    \item Zeige, dass
      \[
        1+rs+(rs)^2+\ldots+(rs)^{n-k-1}\leq\frac 1{1-rs}
      \]
      ist. \emph{Hinweis: Multipliziere mit $1-rs$.}
    \item Folgere, dass $|E|\leq r^k\cdot M\cdot\frac{rs^{k+1}}{1-rs}$ gilt.
    \item Überlege dir, dass es eine Zahl $r>0$ gibt, sodass $|E|\leq|b_0|r^k/2$. \emph{Hinweis: Hier musst du verwenden, dass
      $b_0\neq 0$ ist.}
    \item Zeige, dass für solch ein $r$ gilt: $|q(rx_0)|<|q(0)|$.
  \end{enumerate}
\end{aufgabe}

Also war $|q(0)|$ doch nicht minimal, was ein Widerspruch ist. Es folgt, dass $q(0)=0$ und somit $p(y)=0$ ist, und wir haben eine
Nullstelle gefunden.

\end{document}
