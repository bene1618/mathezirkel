\documentclass{korrespondenz}
\graphicspath{ {../../illustrationen/} }
\usepackage{url}
\usepackage{pgfplots}

\newcommand{\N}{{\mathbb N}}
\newcommand{\Q}{{\mathbb Q}}
\newcommand{\R}{{\mathbb R}}

\newtheorem{dfn}{Definition}

\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Was hat eine Tasse mit einem Donut gemeinsam? Beide haben ein Loch -- bei der Tasse am Henkel, beim Donut in der Mitte. Und,
wenn beide aus sehr elastischem Gummi geformt wären, könnte man sie ineinander deformieren: Nämlich könnte man zuerst die Ränder
der Tasse zusammendrücken, bis im Wesentlichen nur noch der Henkel übrig bleibt. Diesen Henkel kann man dann zu einem Donut
umbauen, indem man ihn etwas symmetrischer und dicker macht.\footnote{Falls du dir die Deformation noch nicht vorstellen
könnt, empfehle ich die Website \url{http://demonstrations.wolfram.com/CoffeeMugToDonut/}.} Das ist ein Beispiel von etwas, das
Topologen einen \emph{Homöomorphismus} nennen. Topologie untersucht diejenigen Eigenschaften von geometrischen Objekten, die
sich unter Homöomorphismen erhalten: Eine solche Eigenschaft ist die Anzahl der Löcher in einer sogenannten \emph{Fläche} wie
der Tasse oder dem Donut, eine andere Eigenschaft wäre beispielsweise die \emph{Orientierbarkeit}, die besagt, ob eine Fläche
eine oder zwei Seiten hat. Etwa hat das Möbiusband nur eine Seite, aber der Zylinder hat zwei.

\begin{figure}[hbp]
  \begin{center}
    \begin{tikzpicture}[baseline=(current bounding box.center)]
      \begin{axis}[hide axis, view={40}{40}]
        \addplot3 [
          surf,
          samples=60,
          samples y=15,
          domain=0:360,
          y domain=-0.5:0.5
        ]
        (
          {(1+0.5*y*cos(x/2))*cos(x)},
          {(1+0.5*y*cos(x/2))*sin(x)},
          {0.5*y*sin(x/2)}
        );
      \end{axis}
    \end{tikzpicture}
    \quad
    \begin{tikzpicture}[yscale=0.9,baseline=(current bounding box.center)]
      \begin{axis}[hide axis, view={40}{40}]
        \addplot3 [
          surf,
          samples=60,
          samples y=15,
          domain=0:360,
          y domain=-0.5:0.5
        ]
        (
          {cos(x)},
          {sin(x)},
          {y}
        );
      \end{axis}
    \end{tikzpicture}
  \end{center}
  \caption{Das Möbiusband (links) hat eine Seite, der Zylinder (rechts) hat zwei.}
\end{figure}

Auf diesem Zirkelzettel wollen wir die Grundbegriffe der Topologie und einige ihrer wichtigsten Resultate kennenlernen.

Der Grundbegriff, um den sich alles in der Topologie dreht, ist der eines \emph{topologischen Raums}. Wie schon oben
beschrieben, will die Topologie nur eher vage Lagebeziehung untersuchen -- insbesondere sind Längen oder Winkel keine
topologischen Invarianten, wohl aber, ob sich ein Punkt "`in der Nähe"' eines anderen Punktes befindet. Dieses Konzept von
"`sich in der Nähe befinden"' hängt aber zunächst einmal doch von einem Längenbegriff ab, den wir ja gerade vermeiden wollen.
Dieses Problem wird gelöst, indem man von \emph{Umgebungen} eines Punktes spricht. Zum Beispiel bezeichnen wir mit $D$ die
Kreisscheibe mit Radius $1$ und Mittelpunkt $M$ in der Ebene. Eine Umgebung von $M$ sollte nun anschaulich etwas sein, das $M$
umschließt, also in ihrem \emph{Inneren} enthält. Der Begriff eines topologischen Raums formalisiert nun Eigenschaften, die man
sich für solche Umgebungen erwarten würde. 

\begin{figure}[htbp]
  \begin{center}
    \begin{tikzpicture}
      \draw[fill=black!12] (0,0) circle (2);
      \draw[black,dashed,fill=black!7] (0,0.6)--(0.3,0.5)--(0.4,-0.1)--(0.2,-0.8)--(-0.3,-0.7)--(-0.9,-0.1)--(-0.8,0.1)--cycle;
      \fill[black] (0,0) node [anchor=east] {$M$} circle (1.5pt);
    \end{tikzpicture}
  \end{center}
  \caption{Kreisscheibe mit Radius $1$ um $M$. Die gestrichelte Linie ist eine Umgebung von $M$.}
\end{figure}

Genauer kann man einen topologischen Raum definieren als eine Menge $X$, und für
jedes Element $p\in X$ einer Menge $\mathcal U_p$ von Teilmengen von $X$.\footnote{Ich weiß, das klingt erst einmal
abschreckend, aber Beispiele folgen bald!} Die Elemente von $\mathcal U_p$ nennt man \emph{Umgebungen} von $p$. Wichtig ist: Die
Elemente von $\mathcal U_p$ sind wieder Mengen, und zwar Teilmengen von $X$, also
\[
  U\in\mathcal U_p\implies U\subset X.
\]
Es gibt auch ein paar Axiome, die sicherstellen, dass der Umgebungsbegriff nicht allzu sehr von dem abweicht, was wir intuitiv
unter einer Umgebung verstehen würden. Und zwar soll für jeden Punkt $p\in X$ gelten:
\begin{itemize}
  \item[(Ü1)] Der Punkt $p$ ist in jeder seiner Umgebungen enthalten, d.h. für alle $U\in\mathcal U_p$ ist $p\in U$,
  \item[(Ü2)] Die ganze Menge $X$ ist eine Umgebung von $p$, also $X\in\mathcal U_p$,
  \item[(Ü3)] Wenn $U\in\mathcal U_p$ eine Umgebung von $p$ ist und $U\subset V\subset X$ gilt, dann ist auch $V$ eine Umgebung von
    $p$, also $V\in\mathcal U_p$.
  \item[(Ü4)] Wenn $U,V\in\mathcal U_p$ Umgebungen von $p$ sind, dann ist auch $U\cap V\in\mathcal U_p$ eine Umgebung von $p$.
\end{itemize}
Nun also zur Zusammenfassung unsere (erste) Definition eines topologischen Raums:

\begin{dfn}
  Ein \emph{topologischer Raum} besteht aus einer Menge $X$ und für jeden Punkt $p\in X$ aus einer Menge $\mathcal U_p$ von
  Teilmengen von $X$, sodass $\mathcal U_p$ die Axiome (Ü1) bis (Ü4) erfüllt.
\end{dfn}

\begin{aufgabe}
  Wie oben bezeichne $D$ die Kreisscheibe in der Ebene. Für jeden Punkt $p\in D$ und jede (beliebig kleine) Zahl $\epsilon>0$ 
  definieren wir die Menge
  \[
    B_\epsilon(p)=\{x\in D:\|x-p\|<\epsilon\}.
  \]
  Es sei $\mathcal U_p$ die Menge derjenigen Teilmengen $U\subset D$ mit der folgenden Eigenschaft: es gibt eine Zahl
  $\epsilon>0$ (die von $U$ abhängen darf), sodass $B_\epsilon(p)\subset U$ ist. 
  
  Überlege dir, dass die so definierte Menge $\mathcal U_p$ von Teilmengen von $D$ die Eigenschaften (Ü1) bis (Ü4) erfüllt.
\end{aufgabe}

\end{document}
