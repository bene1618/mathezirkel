\documentclass{korrespondenz}
\graphicspath{ {./illustrationen/} }

\usepackage{enumitem}

\newcommand{\N}{{\mathbb N}}

\usetikzlibrary{angles,quotes,babel,intersections}

\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Heute möchte ich mit euch basteln. Genauer werden wir \emph{geometrische Konstruktionen mit Origami} durchführen. Aus der Schule
kennt ihr wahrscheinlich ja schon die geometrischen Konstruktionen mit Zirkel und Lineal. Man kann diese Konstruktionen als eine
Art Spiel sehen, bei der in jeder Runde nur eine bestimmte Art Zug erlaubt ist (zum Beispiel, eine Linie durch zwei Punkte zu
ziehen) und dessen Ziel es ist, bestimmte Punkte, Längen, Winkel oder andere geometrische Objekte zu konstruieren.

Beispielsweise kann man, wie ihr wahrscheinlich wisst, folgendermaßen die Mittelsenkrechte zwischen zwei Punkten mit Hilfe von
Zirkel und Lineal konstruieren:

\begin{center}
  \begin{tikzpicture}
    \fill (0,0) circle (2pt);
    \fill (1,0) circle (2pt);
    \draw[gray] (0,0) circle (1);
    \draw[gray] (1,0) circle (1);
    \draw (0.5,-1.9)--(0.5,1.9);
  \end{tikzpicture}
\end{center}

Man zeichnet also jeweils einen Kreis, dessen Mittelpunkt der eine Punkt ist, und der durch den anderen Punkt verläuft. So erhält
man zwei Kreise, und verbindet man die Schnittpunkte dieser beiden Kreise, so erhält man die Mittelsenkrechte.

Wir wollen genau formulieren, was wir mit Konstruktionen mit Origami meinen. Weiter werden wir sehen, dass man sämtliche 
Konstruktionen, die man mit Zirkel und Lineal durchführen kann, auch durch Falten mit Origami erhält. Dabei ist es aber wichtig,
dass wir dabei nur die Punkte, und nicht die Kreise und Linien, als Teil der konstruierten geometrischen Figur ansehen: Kreise kann 
man mit Origami-Faltungen nicht formen (außer vielleicht, man zeichnet sie vor und gibt sich beim Falten sehr viel Mühe). Zuletzt 
werden wir auch Konstruktionen mit Origami beschreiben, die man mit Zirkel und Lineal nicht durchführen kann.

Wir beginnen (sowohl im Fall von Origami als auch im Fall der euklidischen Konstruktionen) mit zwei festen Punkten in der Ebene,
unserem \emph{Einheitsintervall}. Prinzipiell sind diese beiden Punkte beliebig, da alles Weitere relativ zu ihnen passiert -- es
ist nur wichtig, dass es sich um zwei verschiedene Punkte handelt. Wir nennen diese beiden Punkte $0$ und $1$. Jetzt kann man 
die bei euklidischen Konstruktionen erlaubten Schritte folgendermaßen formulieren: 

\begin{enumerate}[label=\textbf{(E\arabic*)}]
  \item Die Punkte $0$ und $1$ sind konstruierte Punkte.
  \item Zwischen zwei konstruierten Punkten $A$ und $B$ kann man die verbindende Gerade $AB$ konstruieren.
  \item Wenn $A$, $B$ und $C$ drei konstruierte Punkte sind, dann ist der Kreis $K_{\overline{BC}}(A)$ mit Mittelpunkt $A$ und
    den Radius $\overline{BC}$ (Abstand zwischen $B$ und $C$) hat.
  \item Wenn $g$ und $h$ zwei konstruierte Geraden sind, die sich schneiden, dann ist ihr Schnittpunkt konstruiert.
  \item Wenn $g$ eine konstruierte Gerade und $K$ ein konstruierter Kreis ist, und wenn sich $g$ und $K$ schneiden, dann sind auch
    die Schnittpunkte von $g$ und $K$ konstruiert.
  \item Wenn $K$ und $L$ zwei konstruierte Kreise sind, die sich schneiden, dann sind auch ihre Schnittpunkte konstruiert.
\end{enumerate}

\begin{aufgabe}
  Wieso ist das Folgende keine Konstruktion der Mittelsenkrechten zwischen $0$ und $1$?

  \begin{center}
    \begin{tikzpicture}
      \fill (0,0) circle (2pt);
      \fill (1,0) circle (2pt);
      \draw[gray] (0,0) circle (0.7);
      \draw[gray] (1,0) circle (0.7);
      \draw (0.5,-1.9)--(0.5,1.9);
    \end{tikzpicture}
  \end{center}
\end{aufgabe}

Um konkrete Aussagen zu treffen, welche Punkte konstruierbar sind, hilft es, sich die Ebene mit kartesischen Koordinaten $(x,y)$
vorzustellen. Der Punkt $0$ soll dann dem Punkt $(0,0)$ in der Ebene entsprechen, und der Punkt $1$ ist gleich dem Punkt $(1,0)$.

\begin{center}
  \begin{tikzpicture}
    \draw[step=.5,gray,very thin] (-2.3,-1.3) grid (2.3,1.3);
    \draw[gray,->] (-2.4,0)--(2.4,0);
    \draw[gray,->] (0,-1.4)--(0,1.4);
    \fill (0,0) circle (2pt) node [anchor=north east] {$0$};
    \fill (1,0) circle (2pt) node [anchor=north] {$1$};
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Beschreibe mit Hilfe der Axiome \textbf{(E1)} bis \textbf{(E5)} genau, wie sich der Punkt $(0,1)$ konstruieren lässt.

  \emph{Tipp: wenn du das zeichnen möchtest, empfiehlt es sich, dass die Punkte $0$ und $1$ einen größeren Abstand als
    $1\,\mathrm{cm}$ haben.}
\end{aufgabe}

Nun wenden wir uns den Origami-Konstruktionen zu. Im Gegensatz zu euklidischen Konstruktionen können hier keine Kreise konstruiert
werden. Die Axiome sind die Folgenden:

\begin{enumerate}[label=\textbf{(O\arabic*)}]
  \item Die Punkte $0$ und $1$ sind konstruierte Punkte.
  \item Zwischen zwei konstruierten Punkten $A$ und $B$ kann man die verbindende Gerade $AB$ konstruieren.
  \item Zwischen zwei konstruierten Punkten $A$ und $B$ kann man die Mittelsenkrechte $M(A,B)$ konstruieren.
  \item Wenn $g$ und $h$ zwei konstruierte Geraden sind, die parallel sind, dann ist auch ihre Mittelparallele $m(g,h)$
    konstruiert.
  \item Wenn $g$ und $h$ zwei konstruierte Geraden sind, die sich in einem Punkt schneiden, dann ist auch die
    Winkelhalbierende $w(g,h)$ konstruiert.
  \item Wenn $P$ und $Q$ zwei konstruierte Punkte und $g$ eine konstruierte Gerade ist, dann kann man diejenige Gerade $k$
    konstruieren, die durch $Q$ verläuft, und sodass die Spiegelung an $k$ den Punkt $P$ auf einen Punkt abbildet, der auf
    $h$ liegt.
  \item Wenn sich zwei konstruierte Geraden $g$ und $h$ schneiden, dann ist auch ihr Schnittpunkt konstruiert.
  \item Wenn $P$ und $Q$ zwei konstruierte Punkte und $g$ und $h$ zwei konstruierte Geraden sind, die sich schneiden, oder wenn
    $g$ und $h$ parallel sind, aber $P\notin g$ oder $Q\notin h$ gilt, dann kann man 
    die eindeutige Gerade $s$ konstruieren, sodass die Spiegelung an $s$ den Punkt $P$ auf die Gerade $g$ und den Punkt $Q$ auf
    die Gerade $h$ abbildet.
\end{enumerate}

\begin{aufgabe}
  Zeichne Bilder für all diese Konstruktionen.
\end{aufgabe}

\begin{aufgabe}
  Überlege dir, wie du die Konstruktionen durch Falten mit einem Blatt Papier (und wieder Aufklappen) realisieren kannst.
\end{aufgabe}

\begin{aufgabe}
  Erkläre, wie man die Konstruktionen \textbf{(O1)} bis \textbf{(O7)} auch mit Hilfe von Zirkel und Lineal (also mit Hilfe der
  Konstruktionen \textbf{(E1)} bis \textbf{(E5)}) durchführen kann.
\end{aufgabe}

\begin{aufgabe}
  Erkläre, wie sich mit Hilfe der Origami-Konstruktionen der Punkt $(0,1)$ konstruieren lässt.
\end{aufgabe}

Als nächstes wollen wir sehen, wie sich alle Punkte aus den euklidischen Konstruktionen mit Hilfe von Origami-Faltungen 
konstruieren lassen. Die Axiome \textbf{(E1)}, \textbf{(E2)} und \textbf{(E4)} tauchen wortwörtlich auch als Origami-Axiome auf --
um die müssen wir uns also nicht kümmern. Die Frage ist also: Wie lassen sich die Kreise ersetzen? Die Lösung hierbei ist, dass
sich Kreise in euklidischen Konstruktionen immer durch drei konstruierte Punkte $A$, $B$ und $C$ beschreiben lassen, nämlich als
$K_{\overline{BC}}(A)$: es gibt nämlich keine andere Vorschrift, um Kreise zu konstruieren. Die Umformulierung des Axioms
\textbf{(E5)} ohne Kreise lautet also:

\begin{enumerate}
  \item[\textbf{(E5')}] Wenn $g$ eine konstruierte Gerade ist und $A$, $B$ und $C$ drei konstruierte Punkte sind, und wenn sich
    die Gerade $g$ von $A$ höchstens den Abstand $\overline{BC}$ hat, dann lassen sich die Punkte auf $g$ konstruieren, die von 
    $A$ genau den Abstand $\overline{BC}$ haben.
\end{enumerate}

\begin{aufgabe}
  Formuliere in einer ähnlichen Weise das Axiom \textbf{(E6)} zu einem Axiom \textbf{(E6')} um.
\end{aufgabe}

\begin{aufgabe}
  Beschreibe, wie man die Axiome \textbf{(E5')} und \textbf{(E6')} aus den Origami-Axiomen herleiten kann. \emph{Warnung: Diese
    Aufgabe ist relativ schwierig. Lasse dich also nicht entmutigen! Versuche am besten, erst auf dem Papier eine Lösung zu
  finden, bevor du diese Lösung mit Hilfe der Origami-Axiome genau aufschreibst.}
\end{aufgabe}

Damit haben wir gesehen, dass man mit Hilfe der Origami-Konstruktionen mindestens so viele Punkte konstruieren kann wie mit Zirkel
und Lineal. Zum Schluss gibt es noch eine Konstruktion, die mit den Origami-Konstruktionen möglich ist, aber nicht mit Zirkel und
Lineal: Die Winkeldrittelung.

Die Ausgangssituation ist dabei die folgende: Wir haben bereits drei Punkte $A$, $B$ und $C$ konstruiert, und wollen den Winkel
$\angle ABC$ bei $B$ dritteln:

\begin{center}
  \begin{tikzpicture}
    \coordinate (A) at (4,0);
    \coordinate (B) at (0,0);
    \coordinate (C) at (1,5);
    \fill (A) circle (2pt) node [below right] {$A$} (B) circle (2pt) node [below] {$B$} (C) circle (2pt) node [above left] {$C$};
    \draw (A)--(B)--(C);
    \pic [draw,->,angle radius=0.8cm] { angle = A--B--C };
  \end{tikzpicture}
\end{center}

Das heißt, wir wollen zwei weitere Geraden durch $B$ zeichnen, sodass alle drei entstehenden Winkel bei $B$ gleich groß sind. Die
Konstruktion verläuft schematisch folgendermaßen:

\begin{enumerate}
  \item Konstruiere die Gerade $g$, die parallel zu $AB$ durch den Punkt $C$ verläuft.
  \item Konstruiere die Mittelparallele $h$ zwischen $g$ und $AB$.
  \item Konstruiere den Lotfußpunkt $P$ von $B$ auf $g$.
  \item Konstruiere die Gerade $k$, sodass die Spiegelung an $k$ den Punkt $P$ auf $BC$ und den Punkt $B$ auf $h$ abbildet.
  \item Dann ist auch der Schnittpunkt von $k$ mit $h$ konstruiert. Wir nennen ihn $Q$.
  \item Konstruiere nun die Gerade $BQ$.
\end{enumerate}

\begin{center}
  \begin{tikzpicture}[scale=1.2]
    \coordinate (A) at (4,0);
    \coordinate (B) at (0,0);
    \coordinate (C) at (1,5);
    \fill (A) circle (2pt) node [below right] {$A$} (B) circle (2pt) node [left] {$B$} (C) circle (2pt) node [below right] {$C$};
    \draw (A)--(B)--(1.2,6);
    \draw[black!65] (-1,5)--(5,5) node [above] {$g$};
    \draw[black!65,name path=h] (-1,2.5)--(5,2.5) node [above] {$h$};
    \draw[black!65] (0,-1)--(0,6);
    \coordinate (P) at (0,5);
    \fill[black!65] (P) circle (2pt) node [below left] {$P$};
    \pic [black!65,draw,"$\cdot$",angle radius=0.4cm] { angle = B--P--C };
    \def\t{0.61}
    \def\s{4.574}
    \draw[dashed,black!65] (0,5)--(0.5+\t,2.5+\t*5) (0,0)--(0.5+\s,2.5);
    \draw[black!65,name path=k] ({0.5*(0.5+\t)},{5+0.5*(-2.5+\t*5)})--({0.5*(0.5+\s)},{0.5*2.5}) node [left] {$k$};
    \path[name intersections={of=k and h,by=Q}];
    \fill[black!65] (Q) circle (2pt) node [left,xshift=-0.5mm,yshift=2.2mm] {$Q$};
    \draw (B)--(Q);
    \pic [draw,angle radius=1cm] { angle = Q--B--C };
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Überlege dir bei jedem Schritt genau, welches Origami-Axiom verwendet wurde. In manchen Schritten brauchst du zwei Axiome, die
  hintereinander ausgeführt werden.
\end{aufgabe}

\begin{aufgabe}
  Begründe, dass du so wirklich den Winkel bei $A$ gedrittelt hast, das heißt, dass $\angle QBC=\frac 13\angle ABC$ ist.
\end{aufgabe}

Hier beenden wir unseren Ausflug in die Welt der Origami-Konstruktionen. Zum Schluss gibt es noch eine Bonus-Aufgabe (die nicht
bei mir abgegeben werden sollte :D). Viel Spaß damit!

\begin{aufgabe}
  Falte als Weihnachtsgeschenk für jemanden, den du magst, eines der folgenden Origamis:
  \begin{itemize}
    \item Stern
    \item Tannenbaum
    \item Schneeflocke
    \item Yoda
  \end{itemize}
  Anleitungen findest du selbst im Internet!
\end{aufgabe}

\end{document}
