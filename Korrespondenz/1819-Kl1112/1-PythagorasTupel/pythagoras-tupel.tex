\documentclass{korrespondenz}
\graphicspath{ {./illustrationen/} }

\newcommand{\N}{{\mathbb N}}

\usetikzlibrary{angles}

\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Auf diesem Zirkelzettel soll es um \emph{pythagoräische Tupel} gehen. Um ein Gefühl dafür zu bekommen, was das ist, beschäftigen
wir uns zuerst einmal mit dem Spezialfall der \emph{pythagoräischen Tripel}. Benannt sind sowohl die Tripel als auch die Tupel
nach dem griechischen Mathematiker Pythagoras von Samos, der im 6.~Jahrhundert vor Christus gelebt hat, beziehungsweise nach dem
bekannten Satz des Pythagoras, der tatsächlich wohl schon lange vor Pythagoras' Lebzeiten bekannt war.

Wie ihr wisst, sagt der Satz des Pythagoras, dass in einem rechtwinkligen Dreieck die Summe der Quadrate der beiden kleineren
Seiten gleich dem Quadrat der größeren Seite ist. Kürzer ausgedrückt wird das üblicherweise mit der Formel $a^2+b^2=c^2$, wobei
$a$, $b$ und $c$ die Längen der in folgendem Bild eingezeichneten Seiten sind.

\begin{center}
  \begin{tikzpicture}
    \coordinate (B) at (0,0);
    \coordinate (A) at (4,3);
    \coordinate (C) at (4,0);
    \draw (A)--(B)--(C)--cycle;
    \path (A) edge node [above left] {$c$} (B);
    \path (B) edge node [below] {$a$} (C); 
    \path (C) edge node [right] {$b$} (A);
    \draw (C) pic [pic text=$\cdot$,draw] { angle=A--C--B };
  \end{tikzpicture}
\end{center}

Ein pythagoräisches Tripel ist jetzt eine mögliche ganzzahlige Kombination dieser Seitenlängen, also beispielsweise $a=3$, $b=4$,
$c=5$.

\begin{aufgabe}
  Finde zwei weitere pythagoräische Tripel.
\end{aufgabe}

\begin{aufgabe}
  Angenommen, du hast ein pythagoräisches Tripel mit rationalen anstatt mit ganzen Zahlen gefunden. Wie kannst du daraus ein
  pythagoräisches Tripel mit ganzen Zahlen erhalten?
\end{aufgabe}

Zum Aufwärmen wollen wir uns überlegen, dass es unendlich viele verschiedene pythagoräische Tripel gibt. Das ist relativ einfach,
wann man die folgende \emph{babylonische Multiplikationsformel} kennt: Für beliebige (reelle) Zahlen $x$ und $y$ gilt nämlich
\begin{equation}
  x\cdot y = \left(\frac{x+y}2\right)^2-\left(\frac{x-y}2\right)^2.\label{eq:babylon-multiplikation}
\end{equation}

\begin{aufgabe}
  Überlege dir, warum die Formel \eqref{eq:babylon-multiplikation} stimmt.
\end{aufgabe}

Mit Hilfe dieser Formel kann man jetzt beliebig viele pythagoräische Tripel konstruieren.

\begin{aufgabe}
  Ersetze in der Formel \eqref{eq:babylon-multiplikation} $x$ durch $u^2$ und $y$ durch $v^2$, stelle die Gleichung um, und
  überlege dir, wie du $a$, $b$ und $c$ in Abhängigkeit von $u$ und $v$ wählen kannst, um pythagoräische Tripel zu erhalten.
  Welche Bedingung müssen $u$ und $v$ dafür erfüllen? Warum gibt es also unendlich viele pythagoräische Tripel?
\end{aufgabe}

Die Formeln für $a$, $b$ und $c$, die du in der letzten Aufgabe erhalten hast, nennt man auch \emph{indische Formeln} für die
pythagoräischen Tripel.

Wir wollen jetzt den Begriff der pythagoräischen Tripel verallgemeinern. Für eine natürliche Zahl $n\in\N$, $n\geq 2$, besteht ein
\emph{pythagoräisches $n$-Tupel} aus Zahlen $x_1,\ldots,x_{n-1},x_n$ mit der Eigenschaft
\begin{equation}
  x_1^2+\ldots+x_{n-1}^2=x_n^2.
\end{equation}

\begin{aufgabe}
  Beschreibe alle pythagoräischen 2-Tupel. Was sind pythagoräische 3-Tupel? Wieso fordern wir $n\geq 2$?
\end{aufgabe}

\begin{aufgabe}
  Finde eine geometrische Interpretation von pythagoräischen 4-Tupeln.
\end{aufgabe}

\begin{aufgabe}
  Finde Beispiele für pythagoräische 4-Tupel, 5-Tupel und 6-Tupel.
\end{aufgabe}

Unser Ziel ist es zu beweisen, dass es für jede Zahl $n\geq 2$ mindestens ein pythagoräisches $n$-Tupel gibt. Bisher wissen wir
das (mit den letzten Aufgaben) für $2\leq n\leq 6$. Jetzt versuchen wir einmal etwas systematischer an dieses Problem
heranzugehen.

\begin{aufgabe}
  Überlege dir, dass es auf jeden Fall unendlich viele pythagoräische $n$-Tupel gibt, wenn $n-1$ eine Quadratzahl ist. (Also 
  $n=2$, $n=5$, $n=10$ etc.)
\end{aufgabe}

\begin{aufgabe}
  Nehmen wir einmal an, dass wir bereits wissen, dass es ein pythagoräisches $n$-Tupel gibt. Überlege dir, dass es dann für jede
  Zahl $k\geq 1$ auch ein pythagoräisches $(k\cdot (n+k-2)+1)$-Tupel gibt. 
  
  \emph{Hinweis: Überlege dir zuerst genau den Fall $k=2$, $n=3$. Du beginnst also mit einem pythagoräischen Tripel, etwa 
    $x_1=3$, $x_2=4$, $x_3=5$. Du musst jetzt dieses Tripel benutzen, um
    Zahlen $y_1$, $y_2$, $y_3$, $y_4$, $y_5$, $y_6$ zu finden, sodass die Summe der Quadrate dieser Zahlen wieder eine Quadratzahl
  ist. Diese Aufgabe ist relativ schwer. Falls du keine Ideen hast, schreibe bitte eine Mail, dann bekommst du weitere Hinweise.}
\end{aufgabe}

% Lösung: $\sum_{i=1}^{n-1}x_i^2=x_n^2\implies\sum_{i=1}^{n-1}x_i^2+(k-1)x_n^2=kx_n^2\implies k\cdot kx_n^2=(kx_n)^2$.

Mit Hilfe der letzten beiden Aufgaben vereinfacht sich die Suche nach pythagoräischen Tupeln enorm.

\begin{aufgabe}
  Verwende die Aufgaben, um dir zu überlegen, dass pythagoräische $n$-Tupel für jedes
  $n\in\{2,3,4,5,6,7,9,10,11,13,15,16,17,19,21,22,23,25,26,27\}$ existieren. Kannst du dieselbe Methode auch für $n=28$ verwenden?
\end{aufgabe}

\begin{aufgabe}
  Finde explizite Beispiele für pythagoräische $n$-Tupel für alle Lücken in der letzten Aufgabe, also für alle
  $n\in\{8,12,14,18,20,24\}$. 
\end{aufgabe}

Jetzt wissen wir, dass es pythagoräische $n$-Tupel mindestens bis $n=27$ gibt. Wir wollen nun eine Konstruktionsmethode für
pythagoräische $n$-Tupel untersuchen, die immer funktioniert, so lange $n$ nur groß genug ist.

Betrachten wir einfach mal den Fall $n=28$. Wir müssen $27$ Zahlen $x_1,\ldots,x_{27}$ finden, sodass die Summe der Quadrate
dieser Zahlen wieder eine Quadratzahl ergibt. Welche Möglichkeiten für Summen von $27$ Quadratzahlen gibt es überhaupt? Die
kleinste mögliche solche Summe erhalten wir in jedem Fall, wenn wir $x_1=\cdots=x_{27}=1$ setzen. Dann ist die Summe der Quadrate
$S=x_1^2+\cdots+x_{27}^2=27$ (was natürlich keine Quadratzahl ist). 

Wir könnten aber auch eine der Zahlen durch eine $2$ ersetzen. In der Summe ersetzen wir dann einen der Summanden $1$ durch eine
$4=2^2$, erhöhen also die Summe insgesamt um $3$. Dieses Verfahren können wir wiederholen bis zu dem Zeitpunkt, an dem wir
sämtliche Einser durch Zweien ersetzt haben. Dann ist die Summe $S=27\cdot 4=108$. Wir können also die Zahlen
$27,30,33,\ldots,105,108$ auf diese Weise erzeugen. Insbesondere ist bei diesen Zahlen auch eine Quadratzahl dabei, nämlich die
$36$ (in Wirklichkeit auch noch eine zweite, nämlich die $81$).

\begin{aufgabe}
  Wie lautet also das entprechende pythagoräische $28$-Tupel konkret?
\end{aufgabe}

Nun könnte man hoffen, dass diese Methode immer funktioniert, aber bereits bei $n=30$ ist keine Quadratzahl im entsprechenden
Bereich dabei.

\begin{aufgabe}
  Überlege dir, dass das stimmt, dass also keine der Zahlen $29,32,\ldots,113,116$ eine Quadratzahl ist.

  \emph{Hinweis: Du musst nicht alle Zahlen aufschreiben, um die Aufgabe zu lösen. Stattdessen kannst du Teilbarkeitstheorie
  verwenden: Welchen Rest müssen Quadratzahlen beim Teilen durch $3$ haben?}
\end{aufgabe}

Wir müssen die Methode also etwas verbessern. Dazu analysieren wir genauer, welche Zahlen wir (speziell im Fall $n=30$) erzeugen
konnten. Und zwar sind das genau die Zahlen zwischen $29$ und $116$, die beim Teilen durch $3$ den Rest $2$ ergeben.
Beispielsweise liegt die Quadratzahl $64$ zwar zwischen $29$ und $116$, hat aber beim Teilen durch $3$ den Rest $1$. Wir müssen
die Summe also so verändern, dass beim Teilen durch $3$ ein anderer Rest, zum Beispiel $1$ herauskommt.

\begin{aufgabe}
  Überlege dir, wie du das anstellen kannst. \emph{Vorsicht, noch nicht weiterlesen: Eine Lösung kommt im nächsten Absatz.}
\end{aufgabe}

Eine Möglichkeit, das zu tun, ist die folgende: Wir ersetzen einfach eine der Einsen durch eine $3$. In der Summe ersetzen wir
dann eine $1$ durch eine $9$, müssen also $8$ dazuzählen. Da $8$ beim Teilen durch $3$ den Rest $2$ ergibt, hat etwa $29+8=37$
beim Teilen durch $3$ den Rest $1$. Wenn wir noch eine weitere $8$ dazuzählen, bekommen wir auch noch diejenigen Zahlen, die durch
$3$ teilbar sind.

\begin{aufgabe}
  In welchem Bereich erhält man mit dieser Methode alle Zahlen (also ohne Lücken)?
\end{aufgabe}

Sobald in diesem Bereich überhaupt eine Quadratzahl vorkommt, sind wir fertig. Und tatsächlich funktioniert diese Methode immer,
wenn $n$ groß genug ist. Um das einzusehen, überlegen wir uns, welche Zahlen wir für eine beliebige Wahl von $n$ mit dieser
Methode erzeugen können. Wir wollen also $n-1$ Quadratzahlen aufsummieren. Ohne das Ersetzen durch Dreier können wir daher die
Zahlen zwischen $n-1$ und $4\cdot(n-1)=4n-4$ erzeugen, die beim Teilen durch $3$ den gleichen Rest wie $n-1$ haben. Ersetzen wir 
eine der Einsen durch eine $3$, dann haben wir eine $1$ weniger, die wir durch eine $2$ ersetzen können. Daher erhalten wir die 
Zahlen zwischen $(n-2)+9=n+7$ und $4\cdot(n-2)+9=4n+1$ mit der richtigen Teilbarkeitsbedingung. Wenn wir noch eine zweite $3$ 
einfügen, dann können wir zusätzlich die Zahlen zwischen $(n-3)+18=n+15$ und $4\cdot(n-3)+18=4n+6$ erzeugen. Auf jeden Fall
bekommen wir also alle Zahlen zwischen $n+15$ und $4n-4$.

Insgesamt haben wir jetzt also folgende Aussage bewiesen:
\emph{Wenn es im Intervall $[n+15,4n-4]$ eine Quadratzahl gibt, dann gibt es ein pythagoräisches $n$-Tupel.}
Unser Ziel ist es nun zu begründen, dass es in diesem Intervall immer eine Quadratzahl gibt, wenn $n$ groß genug ist. Insbesondere
wird $n\geq 28$ ausreichen, sodass wir danach insgesamt bewiesen haben, dass es pythagoräische $n$-Tupel für alle $n\geq 2$ gibt.

Der genaue Beweis funktioniert so. Wir bezeichnen mit $k\in\N$ die größte Zahl, deren Quadrat kleiner ist als $n+15$, also
$k<\sqrt{n+15}$. Wir wollen zeigen, dass $(k+1)^2\in[n+15,4n-4]$ ist, falls $n$ groß genug ist.

\begin{aufgabe}\leavevmode
  \begin{enumerate}
    \item Überlege dir, dass für alle Zahlen $x,y\geq 0$ gilt: $\sqrt{x+y}\leq\sqrt x+\sqrt y$. \emph{Hinweis: Quadriere beide
      Seiten und verwende die binomische Formel.}
    \item Folgere daraus, dass $k<\sqrt n + 4$ gilt.
    \item Zeige, dass $(k+1)^2\leq n+15+2k+1\leq 2n+24$ gilt, falls $n\geq 4$ ist.
    \item Wie groß muss $n$ sein, damit man daraus folgern kann, dass $(k+1)^2\leq 4n-4$ ist?
  \end{enumerate}
\end{aufgabe}

Puh, geschafft! Jetzt gibt es noch ein paar kleine Bemerkungen. Und zwar gibt es erstens den Vier-Quadrate-Satz von Lagrange, der
besagt, dass man jede natürliche Zahl $n\in\N$ sich als Summe von höchstens (aber nicht unbedingt genau) vier Quadratzahlen
schreiben lässt. Außerdem lassen sich fast alle Zahlen als Summe von genau fünf Quadratzahlen schreiben, nämlich alle Zahlen außer
$1$,$2$,$3$,$4$,$6$,$7$,$9$,$10$,$12$,$15$,$18$ und $33$.

Es gibt noch eine andere Verallgemeinerung der pythagoräischen Tripel: Nämlich kann man Zahlen $a,b,c\in\N$ betrachten, die die
Gleichung $a^3+b^3=c^3$ (oder auch mit höheren Exponenten) erfüllen. Der sogenannte \emph{Große Fermatsche Satz} besagt, dass
diese Gleichung nicht für positive ganze Zahlen $a,b,c$ lösbar ist. Obwohl die Aussage dieses Satzes relativ einfach ist, hat es
von seiner Formulierung durch Fermat bis zu seinem Beweis durch Andrew Wiles und Richard Taylorim Jahr 1994 über 350 Jahre 
gedauert, und der entstandene Beweis ist alles andere als einfach.

\end{document}
