\documentclass{korrespondenz}
\graphicspath{ {../../illustrationen/} }

\usetikzlibrary{patterns}

\newcommand{\N}{{\mathbb N}}
\newcommand{\Q}{{\mathbb Q}}
\newcommand{\R}{{\mathbb R}}

\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Das wahrscheinlich bekannteste Beispiel einer Zahlenfolge ist die \emph{Fibonacci-Folge}
\[
  1,1,2,3,5,8,13,21,34,55,\ldots
\]
Hierbei ist jede Zahl die Summe der beiden vorangehenden Zahlen. Andere Beispiele von Folgen sind
\[
  \pi,\pi,\pi,\pi,\ldots
\]
oder
\[
  1,\frac 12,\frac 13,\frac 14,\frac 15,\ldots
\]
Ganz formal versteht man unter einer (reellwertigen) \emph{Folge} eine Abbildung $\N\to\R$. Das heißt, es gibt eine Vorschrift,
die jeder natürlichen Zahl $0,1,2,3,\ldots$\footnote{Ja, meine Konvention der natürlichen Zahlen schließt die $0$ mit ein.} eine
eindeutige reelle Zahl zuordnet. Für das zweite und die dritte Beispiel oben lässt sich so eine Vorschrift auch sehr einfach
angeben: Im zweiten Beispiel wird jeder Zahl $n\in\N$ die Zahl $\pi\in\R$ zugeordnet, und im dritten Beispiel ordnen wir $n\in\N$
die Zahl $\frac 1{n+1}\in\R$ zu. Das Prinzip, mit dem die Fibonacci-Folge konstruiert ist, nennt sich \emph{Rekursion} und ist ein
extrem wichtiges Prinzip bei der Konstruktion von Folgen (und in der Mathematik überhaupt). Wir werden uns weiter unten noch
ausführlicher mit Rekursion beschäftigen.

Folgen sind also Abbildungen -- und Abbildungen werden üblicherweise mit einem Kleinbuchstaben bezeichnet, etwa $f\colon\N\to\R$.
Damit ist gemeint, dass ich eine beliebige natürliche Zahl $n\in\N$ in $f$ einsetzen kann, und dann eine reelle Zahl erhalte, die
man mit $f(n)\in\R$ bezeichnet. Im Spezialfall der Folgen hat es sich eingebürgert, die eingesetzte Zahl als Index unten rechts zu
schreiben, also $f_n=f(n)$. Typische Symbole für Folgen sind $a,b,c$ und $x,y,z$. Die zweite Folge oben könnte man also etwa durch
$a_n=\pi$, und die dritte durch $b_n=\frac 1{n+1}$ beschreiben. Möchte man darauf hinweisen, dass $a$ eine Folge mit den
\emph{Folgengliedern} $a_n$ ist, schreibt man oft auch $a=(a_n)$ oder $a=(a_n)_{n\in\N}$. Beispielsweise könnte man also die
Formulierung \emph{Die Folge $a=(a_n)$ ist gegeben durch $a_n=\frac 1{n+1}$} benutzen.

\begin{aufgabe}
  Beschreibe die Folgen $1,2,3,4,5,\ldots$ und $1,2,4,8,16,32,\ldots$ in dieser Weise.
\end{aufgabe}

Nun zurück zur Fibonacci-Folge und zum Prinzip der Rekursion! Bei der Beschreibung einer Folge ist es wichtig, dass man jedes
Folgenglied eindeutig bestimmen kann, dass man also (wenn die Folge $(a_n)$ ist) für jede Zahl $n\in\N$ die Zahl $a_n$ prinzipiell
angeben kann.\footnote{Das ist so nicht ganz richtig, allerdings handelt es sich hier um Subtilitäten der Logik: Je nach 
  verwendetem Axiomensystem kann man nämlich durchaus Folgen $(a_n)$ beschreiben, bei denen man nicht unbedingt $a_3$ 
  (beispielsweise) angeben kann. Beispielsweise könnte man Folgendes tun: Für jede Zahl $n$ wählt man sich eine Zahl aus der Menge 
  $\{0,\ldots,n\}$ aus und nennt sie $a_n$. Das ist möglich, da ja die Menge $\{0,\ldots,n\}$ für jedes $n\in\N$ mindestens ein 
  Element enthält. Falls ich
  aber nicht genauer spezifiziere, welches Element ich jeweils meine, dann habe ich eine Folge beschrieben, bei der ich noch nicht
  einmal weiß, was $a_1$ ist. Wenn man nichts mit solchen Folgen zu tun haben will, kann man sich beispielsweise bei der
  Konstruktion von Folgen auf \emph{intuitionistische Logik} beschränken. Sich damit zu beschäftigen, würde allerdings hier zu weit 
führen.}
Es muss aber nicht sein, dass man diese Zahl immer durch eine geschlossene Formel wie etwa $a_n=\frac 1{n+1}$ angeben kann. Eine
andere Möglichkeit ist das Prinzip der Rekursion, bei dem man $a_n$ durch eine Formel angibt, die die bisher berechneten Werten
$a_0,\ldots,a_{n-1}$ verwenden kann. Um dann $a_n$ zu berechnen, berechnet man erst $a_0,\ldots,a_{n-1}$, und setzt diese Zahlen
dann in die Formel ein. Die Fibonacci-Folge könnte man dann beispielsweise so beschreiben: \emph{Die Fibonacci-Folge ist die Folge
$(f_n)$ mit}
\[
  f_n=\begin{cases}1,&n\leq 1,\\f_{n-2}+f_{n-1},&n\geq 2.\end{cases}
\]
Beachte hier die Fallunterscheidung, die durch die geschweifte Klammer verdeutlicht wird: Die Zahl $f_{n-2}$ existiert ja
überhaupt nur, falls $n\geq 2$ ist, also muss man für $n\leq 1$ eine andere Definition verwenden. Das reflektiert die Tatsache,
dass wir bei der Fibonacci-Folge zwei Anfangswerte benötigen.

\begin{aufgabe}
  Finde heraus, wie die Folge $2,1,4,7,3,5,32,91,423,13445,\ldots$ gebildet wird, und beschreibe sie mit Hilfe des 
  Rekursionsprinzips.
\end{aufgabe}

\begin{aufgabe}
  Warum ist die Definition $a_n=2a_{n-1}$ keine legitime Anwendung des Rekursionsprinzips?
\end{aufgabe}

Mit Hilfe des Rekursionsprinzips lassen sich auch längere Summen definieren. Dazu betrachten wir irgendeine Folge $(a_n)$. Wir
wollen den Ausdruck $a_0+\ldots+a_n$ formalisieren. Dazu schreiben wir
\[
  \sum_{i=0}^na_i=\begin{cases}a_0,&n=0,\\\sum_{i=0}^{n-1}a_i+a_n,&n\geq 1.\end{cases}
\]
Wenn wir eine Summe der Form $a_0+\ldots+a_n$ schreiben, meinen wir in Wirklichkeit $a_0+\ldots+a_n=\sum_{i=0}^na_i$.

\begin{aufgabe}
  Gib konkrete Formeln für die ersten Folgenglieder $\sum_{i=0}^1a_i$ und $\sum_{i=0}^2a_i$ an, und überlege dir, dass die
  Bezeichnung
  \[
    a_0+\ldots+a_n=\sum_{i=0}^na_i
  \]
  Sinn ergibt.
\end{aufgabe}

\begin{aufgabe}
  Finde eine rekursive Beschreibung des Produkts
  \[
    a_0\cdots a_n=\prod_{i=0}^na_i.
  \]
\end{aufgabe}

Eng verwandt mit dem Konstruktionsprinzip der Rekursion ist das Beweisprinzip der \emph{Induktion}. Hierbei möchte man eine
Aussage für alle Zahlen $n\in\N$ beweisen. Um das zu tun, darf man nun dieselbe Aussage für alle Zahlen $0,\ldots,n-1$ verwenden.
Wie bei der Rekursion muss man typischerweise eine Fallunterscheidung durchführen, um die ersten Fälle zu beweisen. Als Beispiel
wollen wir die Gauß'sche Summenformel
\[
  1+\ldots+n=\frac{n(n+1)}2
\]
beweisen. Die linke Seite ist natürlich formal gegeben als $\sum_{i=0}^na_i$, wobei $(a_n)$ die Folge ist, die durch $a_n=n$
gegeben ist. Da also $a_i=i$ ist, kann man hierfür auch kurz $\sum_{i=0}^ni$ schreiben. Wir wollen also
\[
  \sum_{i=0}^ni=\frac{n(n+1)}2
\]
beweisen. Die linke Seite ist ja durch eine Fallunterscheidung definiert, also müssen wir im Beweis auch eine Fallunterscheidung
durchführen. Falls $n=0$ ist, ist die linke Seite gleich $a_0=0$, genau wie die rechte Seite, sodass die Behauptung in dem Fall
erfüllt ist. Falls $n\geq 1$ ist, ist die linke Seite gleich
\[
  \sum_{i=0}^{n-1}i+n=\frac{(n-1)n}2+n,
\]
wobei wir die Behauptung für die Zahl $n-1\in\{0,\ldots,n-1\}$ verwendet haben. In diesem Fall müssen wir also nur die Formel
\begin{equation}
  \frac{(n-1)n}2+n=\frac{n(n+1)}2\label{eq:gauss-formel}
\end{equation}
beweisen.

\begin{aufgabe}
  Beweise die Formel \eqref{eq:gauss-formel}.
\end{aufgabe}

Warum reicht es nun, eine Aussage mit dem Prinzip der vollständigen Induktion zu beweisen? Die vollständige Induktion
liefert ja zunächst einen Beweis der Aussage für $n=0$. Wenn man bereits weiß, dass die Aussage für $n=0$ gilt, liefert die
vollständige Induktion einen Beweis für $n=1$. Wenn man die Aussage für $n\in\{0,1\}$ bereits kennt, liefert die vollständige
Induktion einen Beweis für $n=2$ und so weiter. Das heißt, für jede konkrete Zahl $n$ erhält man so einen Beweis der Aussage, an
der man interessiert ist.

\begin{aufgabe}\label{aufg:reihen-induktion}
  Beweise mit Hilfe der vollständigen Induktion folgende Formeln:
  \begin{enumerate}
    \item $\sum_{i=0}^ni^2=\frac{n(n+1)(2n+1)}6$,
    \item $(1-q)\sum_{i=0}^nq^i=1-q^{n+1}$ für jede beliebige Zahl $q\in\R$.\footnote{Hierbei ist $(q^n)$ rekursiv definiert.
      Beschreibe, wie! Übrigens ist $q^0=1$.}\label{item:geom-reihe}
    \item $f_n=\frac{\phi^n-\psi^n}{\phi-\psi}$, wobei $(f_n)$ die Fibonacci-Folge ist und
      \[
        \phi=\frac{1+\sqrt 5}2,\quad\psi=\frac{1-\sqrt 5}2
      \]
      die Verhältnisse des goldenen Schnitts sind.
  \end{enumerate}
\end{aufgabe}

Eines der wichtigsten Konzepte bei der Untersuchung von Folgen ist das der \emph{Konvergenz}. Man sagt, eine Folge $(a_n)$
\emph{konvergiert} gegen eine Zahl $a\in\R$, falls sich die Folgenglieder immer stärker an $a$ annähern, falls also $|a_n-a|$
immer näher bei $0$ liegt. In diesem Fall schreibt man $\lim_{n\to\infty}a_n=a$. Hier ist die formale Definition der Konvergenz:
Es ist $\lim_{n\to\infty}a_n=a$ genau dann, wenn für jede (beliebig kleine) Zahl $\epsilon>0$\footnote{In der Mathematik werden
mit $\epsilon$ typischerweise kleine positive Zahlen bezeichnet.} eine Zahl $N\in\N$ existiert, sodass $|a_n-a|\leq\epsilon$ für 
alle $n\geq N$ gilt.

Versuchen wir einmal, diese auf den ersten Blick recht unhandliche Definition zu verstehen. Man kann sich das Ganze als ein Spiel
mit zwei Spielern $A$ und $B$ vorstellen: 
$A$ behauptet, dass $\lim_{n\to\infty}a_n=a$ gilt, und $B$ will diese Aussage widerlegen. Das heißt, $B$ sucht sich
eine beliebig kleine Zahl $\epsilon>0$ aus und gibt sie $A$. Daraufhin muss $A$ eine Zahl $N\in\N$ mitteilen, sodass
$a_N,a_{N+1},a_{N+2},\ldots$ alle höchstens um $\epsilon$ von $a$ entfernt sind.

Man kann sich das Konzept der Konvergenz auch folgendermaßen vorstellen: Und zwar zeichnen wir den Graphen der Folge (erinnere
dich daran, dass eine Folge ja eine Abbildung $\N\to\R$ ist). Das ist die Menge der Punkte mit den Koordinaten $(n,a_n)$ in der
euklidischen Ebene. Wir betrachten zum Beispiel die Folge $(a_n)$, die durch $a_n=\frac 1{n+1}$ gegeben ist. Dann sieht der Graph
folgendermaßen aus:

\begin{center}
  \begin{tikzpicture}
    \draw[->] (-0.5,0)--(9.5,0);
    \draw[->] (0,-0.5)--(0,4.5);
    \draw[help lines] (-0.1,-0.1) grid (9.1,4.1);
    \foreach\i in {1,...,9} {\draw (\i,0.15)--(\i,-0.15) node [below] {$\i$};}
    \foreach\i in {0,...,9} {\fill (\i,{4/(\i+1)}) circle (2pt);}
    \draw (0.15,4)--(-0.15,4) node [left] {$1$};
    \draw (0.15,2)--(-0.15,2) node [left] {$\frac 12$};
  \end{tikzpicture}
\end{center}

Für $\epsilon=\frac 14$ verläuft dieser Graph ab $n=3$ innerhalb des $\epsilon$-Schlauchs um die $0$:

\begin{center}
  \begin{tikzpicture}
    \fill[pattern=north east lines, pattern color=black!50] (-0.1,-1) rectangle (10.1,1);
    \draw[->] (-0.5,0)--(9.5,0);
    \draw[->] (0,-0.5)--(0,4.5);
    \draw[help lines] (-0.1,-0.1) grid (9.1,4.1);
    \foreach\i in {1,...,9} {\draw (\i,0.15)--(\i,-0.15) node [below] {$\i$};}
    \foreach\i in {0,...,9} {\fill (\i,{4/(\i+1)}) circle (2pt);}
    \draw (0.15,4)--(-0.15,4) node [left] {$1$};
    \draw (0.15,2)--(-0.15,2) node [left] {$\frac 12$};
  \end{tikzpicture}
\end{center}

Für $\epsilon=\frac 1{17}$ verläuft der Graph ab $n=16$ innerhalb des $\epsilon$-Schlauchs um die $0$. Allgemeiner verläuft für
jedes $\epsilon=\frac 1N$ der Graph ab $n=N-1$ im $\epsilon$-Schlauch um $0$.

Hier kommt der formale Beweis dafür, dass $\lim_{n\to\infty}a_n=0$: Sei $\epsilon>0$ irgendeine positive Zahl. Wir betrachten
$N=\lceil\frac 1\epsilon\rceil\in\N$, das heißt, die kleinste natürliche Zahl $N$ mit $N\geq\frac 1\epsilon$ (also $\frac
1\epsilon$ aufgerundet). Für alle $n\geq N$ gilt dann:
\[
  |a_n-0|=a_n=\frac 1n\leq\frac 1N\leq\frac 1{1/\epsilon}=\epsilon.
\]
Da $\epsilon$ jede positive Zahl sein kann, gibt es also zu jedem $\epsilon>0$ ein $N\in\N$, sodass für alle $n\geq N$ gilt, dass
$|a_n-0|\leq\epsilon$ ist. Das war genau die Definition von $\lim_{n\to\infty}a_n=0$. Kurz kann man natürlich auch
$\lim_{n\to\infty}\frac 1n=0$ schreiben.

Ein weiteres, etwas langweiliges Beispiel ist die konstante Folge $b_n=\pi$ aus der Einleitung. Hier ist, kaum überraschend,
$\lim_{n\to\infty}b_n=\pi$. Der Grund ist, dass man für jedes $\epsilon>0$ einfach $N=0$ wählen kann. Für alle $n\geq N$ gilt dann
$|b_n-\pi|=|\pi-\pi|=0<\epsilon$.

\begin{aufgabe}\label{aufg:grenzwerte}
  Berechne die Grenzwerte folgender Folgen:
  \begin{enumerate}
    \item Beweise, dass $\lim_{n\to\infty}\frac 1{n^2}=0$ ist. Etwas ausführlicher ausgedrückt: Betrachte die Folge 
      $a_n=\frac 1{n^2}$. Zeige, dass $\lim_{n\to\infty}a_n=0$ ist. Du musst also für ein beliebiges $\epsilon>0$ eine Zahl 
      $N\in\N$ finden, sodass $|b_n-0|\leq\epsilon$ für alle $n\geq N$ gilt.
    \item Beweise, dass $\lim_{n\to\infty}q^n=0$ gilt, falls $0<q<1$ ist.\label{item:exponential-folge-grenzwert}
    \item Sei $(f_n)$ die Fibonacci-Folge. Beweise, dass
      \[
        \lim_{n\to\infty}\frac{f_{n+1}}{f_n}=\phi
      \]
      ist, wobei $\phi$ der goldene Schnitt ist.
  \end{enumerate}
\end{aufgabe}

Die folgende Aufgabe zeigt, dass $\lim_{n\to\infty}b_n$ nicht immer eine Zahl beschreibt, dass also Folgen existieren, die nicht
konvergieren.

\begin{aufgabe}
  Betrachte die Folge $b_n=n$. Zeige, dass diese Folge \emph{nicht} konvergiert, dass es also keine Zahl $b\in\R$ geben kann,
  sodass $\lim_{n\to\infty}b_n=b$ gilt.\footnote{Trotzdem verhält sich die Folge im Grenzwert nicht vollkommen unkontrolliert, 
    sondern "`nähert sich an $\infty$ (unendlich) an"'. Man kann die Aussage $\lim_{n\to\infty}b_n=\infty$ ebenfalls 
    formalisieren. Solche Folgen nennt man \emph{bestimmt divergent}. Für Neugierige: $\lim_{n\to\infty}b_n=\infty$ bedeutet, dass
  für jede Zahl $R<\infty$ eine Zahl $N\in\N$ existiert, sodass $b_n\geq R$ für alle $n\geq N$ gilt.}
\end{aufgabe}

Es gibt ein paar wichtige Rechenregeln in Bezug auf Grenzwerte. Eine davon ist, dass
$\lim_{n\to\infty}(a_n+b_n)=\lim_{n\to\infty}a_n+\lim_{n\to\infty}b_n$ gilt, falls sowohl $\lim_{n\to\infty}a_n$ als auch
$\lim_{n\to\infty}b_n$ existieren. Etwas stärker ausformuliert bedeutet das: Angenommen, wir haben Folgen $(a_n)$ und $(b_n)$ mit
$\lim_{n\to\infty}a_n=a$ und $\lim_{n\to\infty}b_n=b$. Wir betrachten die Folge $c_n=a_n+b_n$. Dann ist
$\lim_{n\to\infty}c_n=a+b$.

Der Beweis geht so: Sei $\epsilon>0$ beliebig. Weil $\lim_{n\to\infty}a_n=a$ ist, existiert eine Zahl $N_a\in\N$, sodass
$|a_n-a|\leq\frac\epsilon 2$ für alle $n\geq N_a$ gilt. Weil $\lim_{n\to\infty}b_n=b$ ist, existiert eine Zahl $N_b\in\N$, sodass
$|b_n-b|\leq\frac\epsilon 2$ für alle $n\geq N_b$ gilt. Wir wählen $N=\max\{N_a,N_b\}$. Für $n\geq N$ gilt dann:
\[
  |c_n-(a+b)|=|(a_n-a)+(b_n-b)|\leq|a_n-a|+|b_n-b|\leq\frac\epsilon 2+\frac\epsilon 2=\epsilon
\]
wegen der Dreiecksungleichung.\footnote{$|x+y|\leq|x|+|y|$ für alle $x,y\in\R$.} Wir haben also gezeigt, dass
$\lim_{n\to\infty}c_n=a+b$ ist.

\begin{aufgabe}
  Beweise, dass $\lim_{n\to\infty}(a_n\cdot b_n)=\lim_{n\to\infty}a_n\cdot\lim_{n\to\infty}b_n$ gilt, falls $(a_n)$ und $(b_n)$
  beide konvergente Folgen sind.
\end{aufgabe}

Wir wenden uns nun sogenannten \emph{Reihen} zu. Dabei handelt es sich um "`unendliche Summen"'. Genauer: Wenn $(a_n)$ eine Folge
ist, dann kann man die zugehörige Reihe
\[
  \sum_{i=0}^\infty a_i
\]
betrachten. Dabei handelt es sich formal gesehen wieder um eine Folge, nämlich um die Folge der \emph{Partialsummen}
\[
  s_n=\sum_{i=0}^na_i.
\]
Falls diese Folge konvergiert, also beispielsweise $\lim_{n\to\infty}s_n=s$ gilt, dann schreiben wir auch
\[
  \sum_{i=0}^\infty s_i.
\]
Es würde den Rahmen dieses Zirkelzettels sprengen, besonders viele Granzwerte von Reihen konkret zu berechnen, daher werden wir
uns auf ein konkretes Beispiel beschränken. Und zwar werden wir die Formel
\[
  \sum_{i=0}^\infty\frac 1{2^i}=2
\]
beweisen. Hierbei handelt es sich um ein Beispiel für die sogenannte \emph{geometrische Reihe}. Wir müssen also die Folge
\[
  s_n=\sum_{i=0}^n\frac 1{2^i}
\]
betrachten und beweisen, dass $\lim_{n\to\infty}s_n=2$ ist. Und das geht so: Nach Aufgabe 
\ref{aufg:reihen-induktion}~\ref{item:geom-reihe} wissen wir, dass
\[
  s_n=\sum_{i=0}^n\left(\frac 12\right)^i=\frac{1-\left(\frac 12\right)^{n+1}}{1-\frac 12}=2-2\cdot\left(\frac
  12\right)^{n+1}
\]
ist. Nach Aufgabe \ref{aufg:grenzwerte}~\ref{item:exponential-folge-grenzwert} wissen wir, dass $\lim_{n\to\infty}\left(\frac
12\right)^{n+1}=0$ gilt.

\begin{aufgabe}
  Benutze diese Informationen, um zu beweisen, dass $\lim_{n\to\infty}s_n=2$ ist, um den Beweis abzuschließen.
\end{aufgabe}

\begin{aufgabe}
  Berechne allgemeiner den Wert der geometrischen Reihe
  \[
    \sum_{i=0}^\infty q^i,
  \]
  falls $0<q<1$ ist.
\end{aufgabe}

Zum Schluss noch wie versprochen ein Wort zur Definition der Vollständigkeit. Eine Folge $(a_n)$ heißt \emph{Cauchy-Folge}, falls
für jede Zahl $\epsilon>0$ eine Zahl $N\in\N$ existiert, sodass $|a_n-a_m|\leq\epsilon$ für alle $n,m\geq N$ gilt. Eine
Cauchy-Folge ist also eine Folge, deren Graph immer konstanter wird, je weiter man nach rechts geht.

Jede konvergente Folge ist eine Cauchy-Folge: Wenn nämlich $\lim_{n\to\infty}a_n=a$ und $\epsilon>0$ eine beliebige positive
Zahl ist, dann gibt es eine Zahl $N\in\N$, sodass $|a_n-a|\leq\frac\epsilon 2$ für alle $n\geq N$ gilt. Für alle $n,m\geq N$ ist
dann
\[
  |a_n-a_m|=|(a_n-a)+(a-a_m)|\leq|a_n-a|+|a-a_m|=|a_n-a|+|a_m-a|\leq\epsilon.
\]
Somit ist $(a_n)$ eine Cauchy-Folge. Umgekehrt gilt: Wenn eine Folge $(a_n)$ eine Cauchy-Folge ist, dann nähert sie sich ja immer
stärker an eine konstante Folge an. Solange also der Zahlenstrahl nicht gerade eine Lücke an dieser Stelle hat, sollte die Folge
konvergieren. Und das ist genau die Charakterisierung der Vollständigkeit der reellen Zahlen: 
\emph{Jede Cauchy-Folge konvergiert}. Etwas ausführlicher: Wenn $(a_n)$ irgendeine Cauchy-Folge ist, dann gibt es eine Zahl 
$a\in\R$, sodass $\lim_{n\to\infty}a_n=a$ ist. Dass das nicht selbstverständlich ist, zeigt das folgende Beispiel, mit dem ich
diesen Zirkelzettel beenden will:

\begin{aufgabe}
  Betrachte die rekursiv definierte Folge $x_n=\begin{cases}1,&n=0,\\\frac{x_{n-1}^2+2}{2x_{n-1}}&n\geq 1.\end{cases}$
  \begin{enumerate}
    \item Beweise, dass $x_n\in\Q$ für alle $n\in\N$ gilt.
    \item Beweise, dass ein Grenzwert $x$ der Folge $(x_n)$ die Eigenschaft $x^2=2$ haben müsste.
    \item Wieso beweist das, dass die rationalen Zahlen nicht vollständig sind?
  \end{enumerate}
\end{aufgabe}

\end{document}
