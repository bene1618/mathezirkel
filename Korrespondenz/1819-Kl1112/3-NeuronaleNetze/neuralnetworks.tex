\documentclass{korrespondenz}
\graphicspath{ {./illustrationen/} }

\usepackage{booktabs}
%\usepackage{hyperref}

\usetikzlibrary{positioning}

\newcommand{\N}{{\mathbb N}}
\newcommand{\R}{{\mathbb R}}


\begin{document}
\maketitle{Klasse 11/12}{\today}{2018/2019}

Auf diesem Zirkelzettel wollen wir uns mit \emph{maschinellem Lernen} -- und speziell mit \emph{neuronalen Netzen} --
beschäftigen. 

Was ist eigentlich maschinelles Lernen? Eine typische Art, ein Computerprogramm zu schreiben, ist folgende: Das Programm liest
eine Eingabe. Mit dieser Eingabe wird jetzt gearbeitet: Zum Beispiel können Fallunterscheidungen durchgeführt oder Berechnungen
angestellt werden. Schließlich errechnet das Programm eine Ausgabe, die geschrieben wird, zum Beispiel auf den Bildschirm. Dieses
Vorgehen ist beispielhaft in der folgenden Grafik dargestellt:

\begin{center}
  \begin{tikzpicture}[node distance=1cm,
    data/.style={
      rectangle, rounded corners=3mm,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    processing/.style={
      rectangle,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (input) [data] {\parbox{2cm}{Eingabe:\\ $n=7$}};
    \node (proc) [processing, right=of input] {\parbox{6cm}{Programm:\\Falls $n<0$:\\\hspace*{1em}Setze $x=\text{"`Fehler!"'}$\\
      Ansonsten:\\\hspace*{1em}Setze $y=100\cdot n$.\\\hspace*{1em}Setze $x=$\\\hspace*{2em}"`Du brauchst $y$ g Mehl."'\\
      Gib $x$ aus.}};
    \node (output) [data,right=of proc] {\parbox{3cm}{Ausgabe:\\Du brauchst\\ $100$ g Mehl.}};

    \path (input) edge [->] (proc) (proc) edge [->] (output);
  \end{tikzpicture}
\end{center}

Dieses Programm erhält als Eingabe die Anzahl $n$ der Gäste auf einer Feier, und gibt aus, wie viel Mehl man für Pizzateig braucht
(falls die Gäste nicht allzu hungrig sind).\footnote{\emph{Rezept für Pizzateig für 4 Personen:} Gib $400$~g Mehl in eine
  Rührschüssel und forme eine kleine Mulde. In die Mulde kommt ein halber Würfel Hefe (ca. $20$~g), zusammen mit einer Prise
  Zucker und lauwarmem Wasser. Die Hefe in dem Wasser auflösen und mit etwas Mehl bedecken. Dieser Vorteig geht jetzt mindestens
  15 Minuten (länger ist auch in Ordnung). Dann eine großzügige Menge Olivenöl (etwa $3$~EL) und etwa $1$~EL Salz in die Schüssel
  geben und das Ganze mit mehr lauwarmem Wasser zu einem Teig verkneten. Du solltest gerade so viel Wasser verwenden, dass sich
  der Teig gut von den Händen löst -- falls der Teig zu klebrig wird, füge einfach Mehl hinzu. Der entstandene Hefeteig muss jetzt
  mit einem Küchenhandtuch abgedeckt mindestens eine halbe Stunde gehen. Danach ausrollen, belegen, und im Ofen bei 250 Grad je
nach Geschmack zwischen 5 und 10 Minuten backen!}

Die Herangehensweise bei maschinellem Lernen ist eine etwas andere: Im einfachsten Fall hat man hier sehr viele Eingaben und
dazupassende Ausgaben vorgegeben und will, dass der Computer selbst lernt, wie er von den Eingaben zu den Ausgaben kommt.
Schematisch sieht das Ganze dann folgendermaßen aus:

\begin{center}
  \begin{tikzpicture}[node distance=3mm,
    data/.style={
      rectangle, rounded corners=3mm,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    processing/.style={
      rectangle,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (input) [data] {Eingabedaten};
    \node (x) [below=of input] {};
    \node (output) [data, below=of x] {Ausgabedaten};
    \node (proc) [processing, right=2cm of x] {\parbox{6cm}{Programm:\\Lies die Eingabedaten und die zugehörigen
      Ausgabedaten.\\Versuche möglichst gut von den Eingabedaten auf die richtigen Ausgabedaten zu kommen.}};

    \path (input.east) edge [->] (proc) (output.east) edge [->] (proc);
  \end{tikzpicture}
\end{center}

Ein typisches Beispiel, wann maschinelles Lernen sinnvoll sein kann, ist Handschriftenerkennung. Es gibt hier ein Beispiel,
welches hier sehr oft herangezogen wird: Die MNIST-Datenbank.\footnote{\emph{MNIST database} steht für \emph{Modified National 
Institute of Standards and Technology database.}} Hier wurden einerseits amerikanische High-School-Studenten und
andererseits Mitarbeiter des United States Census Bureau gebeten, Ziffern von 0 bis 9 in quadratische Felder zu schreiben.
Diese Daten sind unter \texttt{http://yann.lecun.com/exdb/mnist/} öffentlich verfügbar. Das Ganze sieht dann etwa folgendermaßen
aus:

\begin{center}
  \foreach\i in {1,...,14} {\includegraphics{mnist/mnist\i.png}}
  \foreach\i in {15,...,28} {\includegraphics{mnist/mnist\i.png}}
  \foreach\i in {29,...,42} {\includegraphics{mnist/mnist\i.png}}
\end{center}

Das sind die ersten 45 der 60000 Trainingsdaten aus der MNIST-Datenbank. In der Datenbank sind nicht nur diese geschriebenen
Ziffern gespeichert, sondern auch, um welche Ziffer zwischen 0 und 9 es sich hierbei handelt. Es ist nun ziemlich schwer, genau zu
beschreiben, welche Eigenschaften ein Bild haben muss, um beispielsweise als Ziffer 3 klassifiziert zu werden. Daher ist es auch
ziemlich schwierig, ein Programm zu schreiben, welches die Ziffern richtig erkennt.

Eine Lösung liefert hier maschinelles Lernen. Die Struktur eines solchen Programms sieht dann also etwa folgendermaßen aus:

\begin{center}
  \begin{tikzpicture}[node distance=3mm,
    data/.style={
      rectangle, rounded corners=3mm,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    processing/.style={
      rectangle,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (input) [data] {\parbox{3.2cm}{\raggedright Eingabedaten:\\ Bilder von handgeschriebenen Ziffern}};
    \node (x) [below=of input] {};
    \node (output) [data, below=of x] {\parbox{3.2cm}{\raggedright Ausgabedaten:\\Korrekter Wert der jeweiligen Ziffern}};
    \node (proc) [processing, right=2.5cm of x] {\parbox{7cm}{Programm:\\Lies die Bilder und die zugehörigen Ziffern.\\
      Versuche zu lernen, die Ziffern richtig zu erkennen.}};

    \path (input) edge [->] (proc) (output) edge [->] (proc);
  \end{tikzpicture}
\end{center}

Wir wollen versuchen zu verstehen, wie so ein Programm aussehen kann. Konkret werden wir sehen, wie man solch ein Programm als ein
\emph{neuronales Netz} realisieren kann. Die Inspiration für neuronale Netze stammt aus der Physiologie des Gehirns. Im Gehirn
werden Informationen mithilfe von Neuronen verarbeitet. Diese Neuronen sind Verbindungen, die mal stärker und mal schwächer
ausgeprägt sein können.

Unser konkretes Modell für ein Neuron ist das \emph{Sigmoid-Neuron}, das folgendermaßen aussieht:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$x_1$};
    \node (in2) [input, below=of in1] {$x_2$};
    \node (in3) [input, below=of in2] {$\vdots$};
    \node (in4) [input, below=of in3] {$\vdots$};
    \node (in5) [input, below=of in4] {$x_n$};
    \node (neuron) [neuron, right=2cm of in3] {};
    \node (out) [input, right=2cm of neuron] {$y$};
    \foreach\i in {1,...,5} {\path (in\i) edge [->] (neuron);}
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

Ein solches Sigmoid-Neuron (wir schreiben kurz einfach \emph{Neuron}) hat also $n$ Eingabedaten $x_1,x_2,\ldots,x_n$ (wobei $n$ 
jede Zahl sein kann), und produziert einen Ausgabewert $y$. Die Idee dabei ist, dass das Neuron anhand der Eingabedaten
entscheidet, ob eine Aussage wahr oder falsch ist. Für \emph{wahr} steht dabei der Wert $1$, und für \emph{falsch} steht der Wert
$0$.

Diese Entscheidung funktioniert folgendermaßen: jeder der Pfeile von $x_i$ in das Neuron bekommt ein \emph{Gewicht} $w_i\in\R$.
Außerdem erhält das Neuron selbst einen sogenannten \emph{Bias}\footnote{engl. für Voreingenommenheit, Neigung} $b$. Die Idee ist
nun, dass das Neuron die Eingabedaten mit den entsprechenden Gewichten addiert und prüft, ob diese Summe größer oder kleiner als
$-b$ ist.\footnote{das Minuszeichen ist hier nicht so wichtig, die Formel sieht nur später etwas schöner aus.} Falls sie größer
ist, gibt das Neuron die Aussage "`wahr"' aus, und falls sie kleiner ist, die Aussage "`falsch"'.

Wir versuchen, das Ganze in Formeln auszudrücken. Also: Das Neuron berechnet die Summe
\[
  S=w_1\cdot x_1+w_2\cdot x_2+\cdots+w_n\cdot x_n
\]
und gibt $1$ aus, falls $S\geq -b$ ist (also falls $S+b>0$ ist), und $0$, falls $S+b<0$ ist. Zusammengenommen kann man das so
schreiben:
\[
  y=s(S+b)=s\left(w_1\cdot x_1+\cdots+w_n\cdot x_n+b\right)=s\left(\sum_{i=1}^nw_i\cdot x_i+b\right),
\]
wobei
\[
  s(t)=
  \begin{cases}
    1,&t>0,\\0,&t<0
  \end{cases}
\]
die sogenannte \emph{Treppenfunktion} ist. 

\begin{aufgabe}
  Zeichne den Graphen der Treppenfunktion $s$, um zu sehen, weshalb sie diesen Namen hat.
\end{aufgabe}

Den Bias $b$ schreibt man üblicherweise in das Symbol für das Neuron, und die Gewichte $w_i$ schreibt man über die
entsprechenden Pfeile. Ein Beispiel kann also folgendermaßen aussehen:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$7$};
    \node (in2) [input, below=of in1] {$5$};
    \node (in3) [input, below=of in2] {$5$};
    \node (neuron) [neuron, right=2cm of in2] {$2$};
    \node (out) [input, right=2cm of neuron] {$1$};
    \path (in1) edge [->] node [above] {$3$} (neuron);
    \path (in2) edge [->] node [above] {$-1$} (neuron);
    \path (in3) edge [->] node [below] {$-2$} (neuron);
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

Hier haben wir also drei Eingangswerte $x_1=7$, $x_2=5$ und $x_3=5$, sowie die zugehörigen Gewichte $w_1=3$, $w_2=-1$ und
$w_3=-2$. Der Bias ist $b=2$. Daher ist die Ausgabe
\[
  y=s(3\cdot 7+(-1)\cdot 5+(-2)\cdot 5+2)=s(8)=1.
\]

\begin{aufgabe}
  Betrachte das folgende Neuron:
  \begin{center}
    \begin{tikzpicture}[node distance=5mm,
      input/.style={
        rectangle, minimum width=2.5em, minimum height=2em,
        very thick, draw=black!60,
        top color=white, bottom color=black!20
      },
      neuron/.style={
        circle, minimum size=1cm,
        very thick, draw=black,
        top color=white, bottom color=black!20
      }]
      \node (in1) [input] {$3$};
      \node (in2) [input, below=of in1] {$2$};
      \node (in3) [input, below=of in2] {$2$};
      \node (neuron) [neuron, right=2cm of in2] {$-7$};
      \node (out) [input, right=2cm of neuron] {$y$};
      \path (in1) edge [->] node [above] {$-1$} (neuron);
      \path (in2) edge [->] node [above] {$1$} (neuron);
      \path (in3) edge [->] node [below] {$1$} (neuron);
      \path (neuron) edge [->] (out);
    \end{tikzpicture}
  \end{center}
  Was sind in diesem Fall die Eingangswerte und die Gewichte, was ist der Bias, und was ist der Wert von $y$. Wie kannst du das
  erste Gewicht $w_1$ verändern, um einen anderen Wert für $y$ zu erhalten?
\end{aufgabe}

\begin{aufgabe}
  Bevor du weiterliest, überlege dir, welche Nachteile die Funktion $s$ hat.
\end{aufgabe}

Wir werden die Funktion $s$ in Kürze durch eine andere Funktion ersetzen, die ähnlich aussieht, die aber einige Vorteile gegenüber
$s$ hat. Bevor wir das tun, wollen wir aber ein bisschen mehr Intuition für die Neuronen erlangen. Das funktioniert am Leichtesten
mit Hilfe der Funktion $s$, weil diese Funktion sehr einfach ist.

Eine Idee hierbei ist, dass man (wie oben) Wahrheitswerte durch Zahlen modelliert. Dabei entspricht $1$ dem Wahrheitswert "`wahr"'
und $0$ dem Wahrheitswert "`falsch"'. Man kann nun Neuronen benutzen, um verschiedene Wahrheitswerte zu kombinieren. Zum Beispiel
folgendermaßen:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {};
    \node (in2) [below=of in1] {};
    \node (in3) [input, below=of in2] {};
    \node (neuron) [neuron, right=2cm of in2] {$-1.5$};
    \node (out) [input, right=2cm of neuron] {};
    \path (in1) edge [->] node [above] {$1$} (neuron);
    \path (in3) edge [->] node [below] {$1$} (neuron);
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Was sind die verschiedenen Ausgabewerte dieses Neurons, wenn die Eingabewerte nur $0$ oder $1$ sein dürfen?
\end{aufgabe}

Wir setzen also in dieses Neuron zwei Wahrheitswerte ein, und erhalten einen weiteren Wahrheitswert als Ausgabe. Eine solche
Kombination kann man am besten in einer Wahrheitstabelle darstellen:

\begin{center}
  \begin{tabular}{ccc}
    \toprule
    $x_1$&$x_2$&Ergebnis \\
    \midrule
    wahr&wahr&wahr\\
    wahr&falsch&falsch\\
    falsch&wahr&falsch\\
    falsch&falsch&falsch\\
    \bottomrule
  \end{tabular}
\end{center}

Das Ergebnis ist also genau dann wahr, wenn der erste \emph{und} auch der zweite Eingabewert wahr sind. Daher modelliert das
Neuron in diesem Fall die \emph{Und-Funktion}.

\begin{aufgabe}
  Finde Neuronen, die folgende Funktionen modellieren:
  \begin{enumerate}
    \item Die \emph{Negation}, deren Wahrheitstabelle gegeben ist durch:
      \begin{center}
        \begin{tabular}{cc}
          \toprule
          $x_1$&Ergebnis \\
          \midrule
          wahr&falsch\\
          falsch&wahr\\
          \bottomrule
        \end{tabular}
      \end{center}
    \item Die \emph{Oder-Funktion}, deren Wahrheitstabelle gegeben ist durch:
      \begin{center}
        \begin{tabular}{ccc}
          \toprule
          $x_1$&$x_2$&Ergebnis \\
          \midrule
          wahr&wahr&wahr\\
          wahr&falsch&wahr\\
          falsch&wahr&wahr\\
          falsch&falsch&falsch\\
          \bottomrule
        \end{tabular}
      \end{center}
  \end{enumerate}
\end{aufgabe}

Man kann die Ausgaben für Neuronen wieder als Eingaben für andere Neuronen benutzen, also zum Beispiel so:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$2$};
    \node (in2) [input, below=of in1] {$-1$};
    \node (in3) [input, below=of in2] {$3$};
    \node (neuron) [neuron, right=2cm of in2] {$1$};
    \node (mid) [input, right=2cm of neuron] {};
    \path (in1) edge [->] node [above] {$1$} (neuron);
    \path (in2) edge [->] node [above] {$-4$} (neuron);
    \path (in3) edge [->] node [below] {$1$} (neuron);
    \path (neuron) edge [->] (mid);
    \node (in4) [input, below=of mid] {$3$};
    \node (in5) [input, below=of in4] {$1$};
    \node (neuron2) [neuron, right=2cm of in4] {$0$};
    \node (out) [input, right=2cm of neuron2] {};
    \path (mid) edge [->] node [above] {$-1$} (neuron2);
    \path (in4) edge [->] node [above] {$0$} (neuron2);
    \path (in5) edge [->] node [above] {$-5$} (neuron2);
    \path (neuron2) edge [->] (out);
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Welche Zahlen stehen in den beiden freien Feldern?
\end{aufgabe}

So eine Kombination von Neuronen nennt man jetzt ein \emph{neuronales Netz}. Typischerweise würde man das allerdings etwas anders
notieren: Erstens würde man alle Eingaben in die gleiche Spalte schreiben, und zweitens würde man den Ausgabewert eines Neurons
nicht extra aufzeichnen, wenn er wieder als Eingabe für ein anderes Neuron verwendet wird. Dasselbe neuronale Netz wie oben würde
also folgendermaßen aussehen:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$2$};
    \node (in2) [input, below=of in1] {$-1$};
    \node (in3) [input, below=of in2] {$3$};
    \node (neuron) [neuron, right=2cm of in2] {$1$};
    \path (in1) edge [->] node [above] {$1$} (neuron);
    \path (in2) edge [->] node [above] {$-4$} (neuron);
    \path (in3) edge [->] node [below] {$1$} (neuron);
    \node (in4) [input, below=of in3] {$3$};
    \node (in5) [input, below=of in4] {$1$};
    \node (temp) [below=of neuron] {};
    \node (neuron2) [neuron, right=2cm of temp] {$0$};
    \node (out) [input, right=2cm of neuron2] {};
    \path (neuron) edge [->] node [above] {$-1$} (neuron2);
    \path (in4) edge [->] node [above] {$0$} (neuron2);
    \path (in5) edge [->] node [above] {$-5$} (neuron2);
    \path (neuron2) edge [->] (out);
  \end{tikzpicture}
\end{center}

Wie hilft nun so ein neuronales Netz beim maschinellen Lernen? Die Idee ist hierbei, dass man ein ausreichend großes neuronales
Netz nimmt und versucht, die Gewichte und Biases so anzupassen, dass bei möglichst vielen Eingabedaten die Ausgabe die richtige
ist. Im Falle der Klassifikation der Ziffern benötigt man 10 verschiedene Ausgaben, die jeweils bewerten, ob es sich bei der
Ziffer um eine 0 handelt (Ausgabe: "`1"', falls ja, und "`0"', falls nein), ob es sich um eine 1 handelt, ob es sich um eine 2
handelt und so weiter. Sinnvolle Eingabedaten wären dann Werte, ob einzelne Pixel eher schwarz oder eher weiß sind.

Nun kommen wir zu den Nachteilen der Treppenfunktion $s$. Der erste (leicht zu reparierende) Nachteil ist, dass die
Treppenfunktion bei $0$ überhaupt nicht definiert ist, also $s(0)=?$. Das könnte man leicht beheben, indem man beispielsweise
einfach festlegt, dass $s(0)=0$ sein soll. Kommen wir zu dem zweiten, deutlich gravierenderen Nachteil: Indem wir die Gewichte ein
kleines bisschen verändern, kann es sehr plötzlich zu sehr unterschiedlichen Ausgaben kommen (nämlich immer dann, wenn der Wert
$S+b$ das Vorzeichen wechselt). Das macht es sehr schwer, leichte Anpassungen am Netzwerk vorzunehmen. Die Lösung hierbei ist,
eine "`geglättete"' Version der Treppenfunktion zu benutzen, nämlich die \emph{Sigmoid-Funktion} $\sigma$. Diese ist gegeben als
die Funktion
\[
  \sigma\colon\R\to\R,\quad\sigma(t)=\frac 1{1+\exp(-t)}.
\]
Dabei ist $\exp(-t)=e^{-t}$ die \emph{Exponentialfunktion} mit Basis $e\approx 2.71828\ldots$.

\begin{aufgabe}
  Berechne einige Werte von $\sigma$, um einen Graph zu zeichnen, der zeigt, weshalb $\sigma$ eine geglättete Version der
  Treppenfunktion ist.
\end{aufgabe}

Die Definition des Augsabewerts eines Neurons, die wir verwenden werden, ist jetzt
\[
  y=\sigma(S+b)=\frac 1{1+\exp\left(-\sum_{i=1}^nw_i\cdot x_i-b\right)}.
\]
Die genaue Formel ist für uns allerdings nicht allzu wichtig (zumindest nicht, bis wir ein neuronales Netz tatsächlich
programmieren wollen). Sobald wir die Gewichte nun ein kleines bisschen ändern, ändern wir auch die Ausgabe nur ein kleines 
bisschen.

Das Schema eines Programms, das mit einem neuronalen Netz maschinell lernt, sieht dann etwa folgendermaßen aus:

\begin{center}
  \begin{tikzpicture}[node distance=3mm,
    data/.style={
      rectangle, rounded corners=3mm,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    processing/.style={
      rectangle,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (input) [data] {Eingabedaten};
    \node (x) [below=of input] {};
    \node (output) [data, below=of x] {Ausgabedaten};
    \node (proc) [processing, right=2cm of x] {\parbox{8cm}{Programm:\\Verwende die Eingabedaten als Eingabedaten $x_i$ für ein
      neuronales Netz. \\
      Passe die Gewichte und Biases des neuronalen Netzes so an, dass möglichst viele der Ausgaben $y$ des neuronalen Netzes mit 
      den Ausgabedaten übereinstimmen.}};

    \path (input.east) edge [->] (proc) (output.east) edge [->] (proc);
  \end{tikzpicture}
\end{center}

Wie könnte nun so ein neuronales Netz zur Klassifikation von Ziffern aussehen? Der Einfachheit halber wollen wir versuchen ein
neuronales Netz zu konstruieren, das lernt zu entscheiden, ob eine konkrete Ziffer eine 0 darstellt oder nicht. Die Eingabedaten
sind die Farbwerte der einzelnen Pixel 
\[
  x_{1,1},x_{2,1},\ldots,x_{28,1},x_{1,2},x_{2,2},\ldots,x_{28,1},x_{28,2},\ldots,x_{28,28}.\footnote{Die
  Bilder in der MNIST-Datenbank haben 28x28 Pixel.}
\]
Ein erster, naiver Versuch eines neuronalen Netzes könnte nun folgendermaßen aussehen:
\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$x_{1,1}$};
    \node (in2) [input, below=of in1] {$x_{2,1}$};
    \node (in3) [input, below=of in2] {$\vdots$};
    \node (in4) [input, below=of in3] {$\vdots$};
    \node (in5) [input, below=of in4] {$x_{28,28}$};
    \node (neuron) [neuron, right=2cm of in3] {};
    \node (out) [input, right=2cm of neuron] {$y$};
    \foreach\i in {1,...,5} {\path (in\i) edge [->] (neuron);}
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Bevor du weiterliest: Überlege dir, warum das wahrscheinlich nicht besonders gut funktionieren wird.
\end{aufgabe}

Es gibt mehrere Probleme bei diesem Netz (das in Wirklichkeit nur aus einem einzelnen Neuron besteht). Erstens gibt es sehr
wenige Anpassungsmöglichkeiten (nämlich "`nur"' $28\cdot 28+1=785$ Parameter, die geändert werden können). Eine (tatsächlich
bereits relativ effiziente) Möglichkeit ist hier, einfach in der ersten Ebene relativ viele Neuronen mit denselben Eingabedaten zu
füttern. Das ganze sieht dann so aus:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (in1) [input] {$x_{1,1}$};
    \node (in2) [input, below=of in1] {$x_{2,1}$};
    \node (in3) [input, below=of in2] {$\vdots$};
    \node (in4) [input, below=of in3] {$\vdots$};
    \node (in5) [input, below=of in4] {$x_{28,28}$};
    \node (neuron1) [neuron, right=2cm of in3] {};
    \node (neuron2) [neuron, above=of neuron1] {};
    \node (neuron3) [neuron, above=of neuron2] {};
    \node (neuron4) [neuron, below=of neuron1] {};
    \node (neuron5) [neuron, below=of neuron4] {};
    \node (neuron) [neuron, right=2cm of neuron1] {};
    \node (out) [input, right=2cm of neuron] {};
    \foreach\i in {1,...,5} {\foreach\j in {1,...,5} {\path (in\i) edge [->] (neuron\j);}}
    \foreach\j in {1,...,5} {\path (neuron\j) edge [->] (neuron);}
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

Zweitens -- und das ist
noch wichtiger -- werden hier die Lagebeziehungen zwischen den einzelnen Pixeln überhaupt nicht beachtet. Allerdings ist es
offenbar für die Ziffern sehr wichtig, welche Pixeln neben welchen anderen liegen.

\begin{aufgabe}
  Versuche, ein neuronales Netz zu gestalten, das die Information über diese Lagebeziehungen miteinbezieht.
\end{aufgabe}

Eine Lösung, die die Lagebeziehungen miteinbezieht, soll hier am Beispiel eines deutlich kleineren Bilds mit 3x3 Pixeln
verdeutlicht werden. Das Bild sieht also so aus:

\begin{center}
  \begin{tikzpicture}
    \foreach\i in {1,2,3}
    {
      \foreach\j in {1,2,3}
      {
        \draw[thick] (\i-1,\j-1) rectangle (\i,\j);
        \node at ({\i-0.5},{\j-0.5}) {$x_{\i,\j}$};
      }
    }
  \end{tikzpicture}
\end{center}

Dabei geben die Zahlen $x_{1,1},\ldots,x_{3,3}$ die Farbwerte des entsprechenden Pixels an, etwa auf einer Skala von $0$ bis $1$.
Nun werden in einem ersten Schritt immer vier angrenzende Pixel zu einem Punkt kombiniert. Schematisch sieht das etwa so aus:

\begin{center}
  \begin{tikzpicture}
    \fill[black!20] (0,0) rectangle (2,2);
    \foreach\i in {1,2,3}
    {
      \foreach\j in {1,2,3}
      {
        \draw[thick] (\i-1,\j-1) rectangle (\i,\j);
      }
    }
    \fill[black!20] (4.5,0.5) rectangle (5.5,1.5);
    \foreach\i in {1,2}
    {
      \foreach\j in {1,2}
      {
        \draw[thick] (\i+3.5,\j-0.5) rectangle (\i+4.5,\j+0.5);
      }
    }
    \draw (0,0)--(4.5,0.5) (2,0)--(5.5,0.5) (0,2)--(4.5,1.5) (2,2)--(5.5,1.5);
  \end{tikzpicture}
\end{center}

Das zugehörige neuronale Netz wäre dann das folgende:

\begin{center}
  \begin{tikzpicture}[node distance=5mm,
    input/.style={
      rectangle, minimum width=2.5em, minimum height=2em,
      very thick, draw=black!60,
      top color=white, bottom color=black!20
    },
    neuron/.style={
      circle, minimum size=1cm,
      very thick, draw=black,
      top color=white, bottom color=black!20
    }]
    \node (x11) [input] {$x_{1,1}$};
    \node (x12) [input,below=of x11] {$x_{1,2}$};
    \node (x13) [input,below=of x12] {$x_{1,3}$};
    \node (x21) [input,below=of x13] {$x_{2,1}$};
    \node (x22) [input,below=of x21] {$x_{2,2}$};
    \node (x23) [input,below=of x22] {$x_{2,3}$};
    \node (x31) [input,below=of x23] {$x_{3,1}$};
    \node (x32) [input,below=of x31] {$x_{3,2}$};
    \node (x33) [input,below=of x32] {$x_{3,3}$};
    \node (n11) [neuron,right=2cm of x12] {};
    \node (n12) [neuron,right=2cm of x21] {};
    \node (n21) [neuron,right=2cm of x23] {};
    \node (n22) [neuron,right=2cm of x32] {};
    \foreach\i in {11,12,21,22} {\path (x\i) edge [->] (n11);}
    \foreach\i in {12,13,22,23} {\path (x\i) edge [->] (n12);}
    \foreach\i in {21,22,31,32} {\path (x\i) edge [->] (n21);}
    \foreach\i in {22,23,32,33} {\path (x\i) edge [->] (n22);}
    \node (neuron) [neuron,right=5cm of x22] {};
    \foreach\j in {11,12,21,22} {\path (n\j) edge [->] (neuron);}
    \node (out) [input,right=2cm of neuron] {};
    \path (neuron) edge [->] (out);
  \end{tikzpicture}
\end{center}

\begin{aufgabe}
  Überlege dir, wie viele Ebenen von hintereinandergeschalteten Neuronen man braucht, wenn man dieses Konzept auf unseren Fall von
  28x28-Bildern übertragen will.
\end{aufgabe}

\begin{aufgabe}
  Wie viele Parameter hätte man in diesem Fall?
\end{aufgabe}

\begin{aufgabe}
  Überlege dir andere Möglichkeiten für neuronale Netze, um das Klassifikationsproblem für Ziffern zu lösen.
\end{aufgabe}

\begin{aufgabe}
  Was, denkst du, ist ein Nachteil von neuronalen Netzen beziehungsweise maschinellem Lernen? Überlege dir einen Bereich, in dem
  du eher kein maschinelles Lernen verwenden würdest.
\end{aufgabe}

\end{document}
