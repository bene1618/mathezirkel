\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{korrespondenz}[2018/10/22 XeLaTeX class]

\LoadClass[a4paper,parskip=half]{scrartcl}

\RequirePackage{fontspec}
\RequirePackage{polyglossia}
\RequirePackage{amsmath,amsthm,amssymb,graphicx}
\RequirePackage[protrusion=true]{microtype}
\RequirePackage{geometry}
\RequirePackage{tikz}
\RequirePackage{unicode-math}
\RequirePackage{xstring}

\graphicspath{ {./illustrationen/} }

\usetikzlibrary{calc}
\setmainfont{TeX Gyre Bonum}
\setmathfont{TeX Gyre Bonum Math}
\setmonofont{Linux Libertine Mono O}
\setdefaultlanguage[babelshorthands=true]{german}

\setlength{\unitlength}{1cm}

\parindent0em

\newlength{\titleskip}
\setlength{\titleskip}{1.3em}
\renewcommand{\maketitle}[3]{%
  \begin{picture}(0,0)
    \put(0,-1) { \vbox{ Matheschülerzirkel\\Universität Augsburg\\Schuljahr #3\\#1 }}
    \put(13,-1.7) { \includegraphics[height=8em]{cover} }
  \end{picture}
  \vspace*{1.7cm}
  \begin{center}\Large\textbf{Zirkelzettel vom #2}\end{center}

  \vspace{\titleskip}
}

\newcommand{\makeinfotitle}[3]{%
  \begin{picture}(0,0)
    \put(0,-1) { \vbox{ Matheschülerzirkel\\Universität Augsburg\\Schuljahr #3\\#1 }}
    \put(13,-1.7) { \includegraphics[height=8em]{cover} }
  \end{picture}
  \vspace*{1.7cm}
  \begin{center}\Large\textbf{#2}\end{center}

  \vspace{\titleskip}
}

\newcommand{\makeletterhead}[5]{% 
% Die Parameter hier sind:
% 1. Name Zirkelleiter
% 2. Mail Zirkelleiter
% 3. Datum (normalerweise \today)
% 4. Anschrift
% 5. Betreff
  \begin{picture}(0,3.5)
    \put(0,0){\vbox{
      \begin{tabbing}
        {\tiny Uni Augsburg -- Matheschülerzirkel -- Inst. f. Math. -- 86135 Augsburg} \\
        #4
      \end{tabbing}
    }}
    \put(9,0){\vbox{
      \small
      \begin{tabbing}
        \textbf{#1} \\
        \ \\
        Matheschülerzirkel \\
        Universität Augsburg \\
        Institut für Mathematik \\
        86135 Augsburg
      \end{tabbing}
    }}
    \put(9,-2){\vbox{
      \small
      \begin{tabbing}
        {#2} \\
        \rule{165pt}{0.25mm} \\
        Augsburg, den #3
      \end{tabbing}
    }}
    \put(13.3,-0.5) { \includegraphics[height=8em]{cover} }
    \put(8.9,4.2) { \includegraphics[width=15em]{logo-ifm} }
  \end{picture}
  \vspace*{2.5cm}

  \textbf{#5}\par
  \vspace{\titleskip}
}

\def\chopline#1;#2;#3;#4;#5;#6;#7;#8 \\{
  \def\vorname{#2}
  \def\nachname{#3}
  \def\gender{#4}
  \def\strasse{#6}
  \def\plz{#7}
  \def\ort{#8}
}

\newif\ifmore\moretrue

\newcommand{\serienbrief}[6]{% 1. Parameter ist der Dateiname der Adressliste, danach kommen Parameter 
                             % 1,2,3,5 von \makeletterhead, und als letztes Argument der Text des Briefs
\pagestyle{empty}
\newif\ifganzeklasse
\newif\ifweiblich
\newread\liste
\openin\liste=#1
\read\liste to \zeile
\loop
\read\liste to \zeile
\ifeof\liste\global\morefalse\else
\expandafter\chopline\zeile\\ % Hier wird die Zeile in ihre Bestandteile zerlegt
\IfSubStr{\gender}{x}{\ganzeklassetrue}{\ganzeklassefalse}
\IfSubStr{\gender}{w}{\weiblichtrue}{\weiblichfalse}
\ifganzeklasse
  \def\anschrift{\vorname\\ \nachname \\ \strasse \\ \plz~\ort}
  \makeletterhead{#2}{#3}{#4}{\anschrift}{#5}
\else
  \def\anschrift{\vorname~\nachname \\ \strasse \\ \plz~\ort}
  \makeletterhead{#2}{#3}{#4}{\anschrift}{#5}
\fi
\ifganzeklasse Liebe Schülerinnen und Schüler,\else
\ifweiblich Liebe\else Lieber\fi{} \vorname,\fi
\par
#6
\newpage
\fi\ifmore\repeat
\closein\liste
}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand{\labelenumi}{\theenumi)}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\newtheoremstyle{aufgabe}
  {2ex} %spaceabove
  {2ex} %spacebelow
  {} %bodyfont
  {} %indent
  {} %headfont
  {} %headpunctuation
  {0ex} %headspace
  {{\bfseries\thmname{#1}\thmnumber{ #2}.}\thmnote{ \itshape #3}\\[\parskip]} %headspec

\newtheoremstyle{aufgabe*}
  {2ex} %spaceabove
  {2ex} %spacebelow
  {} %bodyfont
  {} %indent
  {} %headfont
  {} %headpunctuation
  {0ex} %headspace
  {{\bfseries\thmname{#1}\thmnumber{ #2}.}\thmnote{ \itshape #3}} %headspec

\theoremstyle{aufgabe}
\newtheorem{aufgabe}{Aufgabe}

\theoremstyle{aufgabe*}
\newtheorem{aufgabe*}[aufgabe]{Aufgabe}
